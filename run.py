from sett.gui.__main__ import run
import multiprocessing

if __name__ == "__main__":
    # On Windows calling this function is necessary.
    # On Linux/OSX it does nothing.
    multiprocessing.freeze_support()
    run()
