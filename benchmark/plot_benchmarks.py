#!/usr/bin/env python3

# pylint: disable=missing-module-docstring
# pylint: disable=invalid-name
import os
import re
import math
import logging
import argparse
from typing import Sequence, Tuple, Any
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from run_benchmarks import load_benchmark_config, FileDataType, FileSizeType
from sett import APP_NAME_SHORT


# File data type description.
DATA_TYPE_DESCRIPTION = {
    FileDataType.TEXT.value: "FASTA-format text files",
    FileDataType.BINARY.value: "random binary files (simulates already compressed data)",
}

# Prefix for plot files saved to disk.
DEFAULT_PLOT_PREFIX = "bm_plot"


def load_benchmark_results_file(path: str) -> pd.DataFrame:
    """Load a sett benchmark output file and remove unnecessary columns."""

    # Load data and remove "[MB]" and "[sec]" strings from column names.
    df_input: pd.DataFrame = pd.read_table(path, header=0)
    df_input.rename(
        columns={
            x: re.sub(r"\[(MB|sec)\]$", "", str(x))
            for x in df_input.columns  # pylint: disable=no-member
        },
        inplace=True,
    )
    df_input.rename(  # pylint: disable=no-member
        columns={"total_runtime": "total_runtimes"}, inplace=True
    )

    # Delete useless columns.
    cols_to_remove = [
        "datetime_completed",
        "file_composition",
    ] + [
        x
        for x in df_input.columns  # pylint: disable=no-member
        if str(x).endswith("_min")
        or str(x).endswith("_max")
        or str(x).endswith("_runtimes")
    ]
    for col_name in cols_to_remove:
        del df_input[col_name]  # pylint: disable=unsupported-delete-operation

    return df_input


def get_df_column_unique_value(df: pd.DataFrame, col_name: str) -> Any:
    """Get the unique value from a DataFrame column that should contain only
    the same values.
    """
    unique_value = df[col_name].unique()
    if len(unique_value) > 1:
        raise ValueError(f"Multiple values found in column [{col_name}] of DataFrame")

    return unique_value[0]


def array_unique_values_as_str(array: "pd.Series[Any]") -> str:
    """Extracts unique values from an array (e.g. DataFrame column) and returns
    them as a string.
    """
    return ", ".join(str(x) for x in array.unique())


def log_data_summary(input_df: pd.DataFrame, file_name: str) -> None:
    """Print input data summary to logger."""

    logging.info(
        "Benchmark factor levels for file %s:\n"
        "Data size levels:           %s\n"
        "Data type levels:           %s\n"
        "File size type levels:      %s\n"
        "Compression methods levels: %s\n"
        "Number of replicates:       %s\n"
        "sett version:               %s\n",
        os.path.basename(file_name),
        array_unique_values_as_str(input_df["data_size"]),
        array_unique_values_as_str(input_df["data_type"]),
        array_unique_values_as_str(input_df["file_size_type"]),
        array_unique_values_as_str(input_df["compression_cmd"]),
        get_df_column_unique_value(df=input_df, col_name="replicates"),
        get_df_column_unique_value(df=input_df, col_name="sett_version"),
    )


def megabytes_as_string(size_in_megabytes: int, sep: str = " ") -> str:
    """Converts a number of megabytes into human readable units.

    :param size_in_megabytes: megabyte value to convert to string.
    :param sep: separator string to use between the size value and the unit.
        e.g. if sep = " " -> "3 MB"
    :return: size as human readable string.
    """
    if size_in_megabytes < 1000:
        units = "MB"
    else:
        units = "GB"
        size_in_megabytes = round(size_in_megabytes / 1000)

    return str(size_in_megabytes) + sep + units


def compression_cmds_colors(compression_cmds: Sequence[str]) -> Tuple[str, ...]:
    """Color scheme usedin plots: colors associated with each compression
    command.
    """
    color_scheme = {
        "sett-1cpu": "seagreen",
        "pigz-1cpu": "orange",
        "pigz-2cpu": "gold",
        "pigz-4cpu": "orangered",
        "pigz-8cpu": "firebrick",
        "pigz-16cpu": "purple",
    }
    return tuple(color_scheme.get(x, "grey") for x in compression_cmds)


def lineplot_total_time_by_data_size(
    input_df: pd.DataFrame,
    data_type: FileDataType,
    file_size_type: FileSizeType,
    compression_level: int,
    output_file_prefix: str = DEFAULT_PLOT_PREFIX,
    show_in_console: bool = False,
) -> None:
    """Plot the total encryption, transfer and decryption time as function of
    input data size.
    """

    # Convert data size values to GB, task duration times to minutes.
    df = input_df.loc[
        (input_df["data_type"] == data_type.value)
        & (input_df["file_size_type"] == file_size_type.value),
        :,
    ].copy()
    df["data_size"] /= 1000
    df[["encrypt_time_mean", "transfer_time_mean", "decrypt_time_mean"]] /= 60

    # Get levels of compression commands in the DataFrame.
    compression_cmds = tuple(df["compression_cmd"].unique())

    # Add data to plot: one line per compression command.
    sns.set(style="ticks")
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot()
    for compression_cmd in compression_cmds:
        mask = (
            (df["compression_cmd"] == compression_cmd)
            & (df["compression_level"] == compression_level)
            & (df["data_type"] == data_type.value)
        )
        ax.plot(
            df.loc[mask, "data_size"],
            df.loc[mask, "encrypt_time_mean"],
            marker="o",
            markersize=5,
            color=compression_cmds_colors((compression_cmd,)),
            label=f"Compression+Encryption {compression_cmd}",
        )

    # Add data to plot: one line for transfer time and one for decryption time.
    for cmd in ("transfer", "decrypt"):
        mask = (
            (df["compression_cmd"] == compression_cmds[0])
            & (df["compression_level"] == compression_level)
            & (df["data_type"] == data_type.value)
        )
        ax.plot(
            df.loc[mask, "data_size"],
            df.loc[mask, cmd + "_time_mean"],
            marker="o",
            markersize=5,
            linestyle="dashed",
            label="Transfer (on local host)" if cmd == "transfer" else "Decryption",
        )

    # Add title to plot.
    # Note: "subplots_adjust()" allows to set the size of the figure dedicated
    # to the graph (as opposed to title space) as a fraction of the total
    # figure size. The default value is "0.9".
    plt.subplots_adjust(top=0.82, left=0.08, right=0.96, bottom=0.1)
    plt.title(
        "Time to complete sett tasks as a function of input data size",
        size=16,
        ha="center",
        y=1.18,
    )
    plt.suptitle(
        f"Input data: {DATA_TYPE_DESCRIPTION[data_type.value]} "
        f"- {file_size_type.value} file sizes\n"
        f"gzip compression level: {compression_level}\n"
        f"sett version: {get_df_column_unique_value(df, 'sett_version')}\n"
        f"Values are means across {get_df_column_unique_value(df, 'replicates')} "
        f"replicates (variance is smaller than dot symbols in the graph)\n",
        ha="center",
        size=12,
        y=0.94,
    )

    # Add legend and axis labels.
    plt.xlabel("Input data size [GB]", size=14, labelpad=15)
    plt.ylabel("Time [minutes]", size=14, labelpad=15)
    plt.legend(
        loc="best",
        frameon=False,
        prop={"size": 12},
        title="sett task",
        title_fontsize=14,
        bbox_to_anchor=(0.4, 0.97),
    )
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)

    # Save plot to file.
    plt.savefig(
        f"{output_file_prefix}_task_time_vs_data_size_{data_type.value}_"
        f"compressionlevel_{compression_level}.png",
    )
    if show_in_console:
        plt.show()


def lineplot_total_time_by_compression_level(
    input_df: pd.DataFrame,
    data_size: int,
    data_type: FileDataType,
    file_size_type: FileSizeType,
    output_file_prefix: str = DEFAULT_PLOT_PREFIX,
    show_in_console: bool = False,
) -> None:
    """Plot the total encryption, transfer and decryption time as function of
    compression level.
    """
    # Subset input data to keep only the rows needed. Convert time to minutes.
    df = input_df.loc[
        (input_df["data_size"] == data_size)
        & (input_df["data_type"] == data_type.value)
        & (input_df["file_size_type"] == file_size_type.value),
        :,
    ].copy()
    df["total_time_mean"] /= 60

    # Add data to plot.
    sns.set_style("ticks")
    g = sns.FacetGrid(
        data=df,
        hue="compression_cmd",
        height=12,
        palette=compression_cmds_colors(tuple(df["compression_cmd"].unique())),
    )
    g.map(plt.scatter, "compression_level", "total_time_mean")
    g.map(plt.plot, "compression_level", "total_time_mean")

    # Modify x/y axis ticks values.
    x_ticks = range(0, 10)
    y_ticks = range(0, math.ceil(df["total_time_mean"].max() / 2) * 2 + 2, 2)
    g.ax.set_xticks(x_ticks)
    g.ax.set_yticks(y_ticks)

    # Add x/y axis labels, title and subtitle to plot.
    plt.subplots_adjust(top=0.85, left=0.08, bottom=0.08, right=0.95)
    g.ax.set_title("Total time vs. compression level\n", size=18, y=1.12, ha="center")
    plt.suptitle(
        f"total time = compression and encryption + transfer + decryption\n"
        f"Input data: {DATA_TYPE_DESCRIPTION[data_type.value]} - "
        f"{megabytes_as_string(data_size)} - {file_size_type.value} file sizes\n"
        f"sett version: {get_df_column_unique_value(df, 'sett_version')}\n"
        f"Values are means across {get_df_column_unique_value(df, 'replicates')} "
        f"replicates",
        y=0.95,
        x=0.55,
        ha="center",
        size=14,
    )
    g.ax.set_xlabel("gzip compression level", size=15, labelpad=12)
    g.ax.set_ylabel("Total time [minutes]", size=15, labelpad=12)

    # Add legend to plot. Note that because seaborn adds handles for both the
    # scatter and lines, we have to manually keep only part of the handles/labels.
    handles, labels = g.ax.get_legend_handles_labels()
    plt.legend(
        handles=handles[0:4],
        labels=labels[0:4],
        loc="best",
        frameon=False,
        prop={"size": 14},
        title="Compression command",
        title_fontsize=15,
    )

    # Save figure to disk.
    g.savefig(
        f"{output_file_prefix}_total_time_vs_compression_level_"
        f"{megabytes_as_string(data_size, sep='')}.png"
    )
    if show_in_console:
        plt.show()


def data_summary_str(df: pd.DataFrame) -> str:
    """Multi-line data summary string to be used in plots subtitles"""
    return (
        f"Input data type: "
        f"{DATA_TYPE_DESCRIPTION[get_df_column_unique_value(df, 'data_type')]}\n"
        f"Input data size: {array_unique_values_as_str(df['data_size'])} MB\n"
        f"Input file size type: {array_unique_values_as_str(df['file_size_type'])}\n"
        f"sett version: {get_df_column_unique_value(df, 'sett_version')}\n"
        f"Values are means across {get_df_column_unique_value(df, 'replicates')} "
        f"replicates for each input data factor combination"
    )


def add_legend(ax: plt.axis, bbox_to_anchor: Tuple[float, float]) -> None:  # type: ignore
    """Add legend of compression commands to plot"""
    ax.legend(  # type: ignore
        loc="best",
        frameon=False,
        prop={"size": 14},
        title="Compression command",
        title_fontsize=15,
        bbox_to_anchor=bbox_to_anchor,
    )


def barplot_sett_output_size_by_compression_level(
    input_df: pd.DataFrame,
    data_type: FileDataType,
    output_file_prefix: str = DEFAULT_PLOT_PREFIX,
    show_in_console: bool = False,
) -> None:
    """Barplot of sett output size as function of compression level"""

    # Subset input data frame to keep only the rows needed.
    df = input_df.loc[input_df["data_type"] == data_type.value, :].copy()

    # Compute compression ratio: the output size as a fraction of the input
    # size (e.g. 0.3 = data size was reduced by 70%).
    df["compression_ratio"] = [
        x if x <= 1 else 1 for x in df["output_size"] / df["data_size"]
    ]

    # Draw plot.
    sns.set(style="whitegrid")
    with sns.plotting_context("notebook", font_scale=1.2):
        g = sns.catplot(
            x="compression_level",
            y="compression_ratio",
            data=df,
            hue="compression_cmd",
            kind="bar",
            height=12,
            palette=compression_cmds_colors(tuple(df["compression_cmd"].unique())),
            legend_out=False,
        )

    # Add axis labels, title, subtitle and legend to plot.
    plt.subplots_adjust(top=0.85, left=0.08, bottom=0.08, right=0.95)
    g.ax.set_xlabel("gzip compression level", size=15, labelpad=12)
    g.ax.set_ylabel(
        "Compression ratio [compressed file size as % of original file]",
        size=15,
        labelpad=12,
    )
    plt.title("Compression ratio vs. compression level for text files", size=18, y=1.15)
    plt.suptitle(data_summary_str(df), y=0.96, x=0.5, ha="center", size=14)
    add_legend(ax=g.ax, bbox_to_anchor=(0.97, 0.95))

    # Add mean compression ratio value numbers on top of each bar group.
    for x, y in enumerate(
        map(
            lambda x: round(
                df.loc[df["compression_level"] == x, "compression_ratio"].mean(), 2
            ),
            range(10),
        )
    ):
        g.ax.text(x - 0.15, y + 0.02, y, fontsize=12)

    # Save figure to disk.
    g.savefig(f"{output_file_prefix}_compression_ratio_vs_compression_level.png")
    if show_in_console:
        plt.show()


def barplot_compression_per_time_unit(
    input_df: pd.DataFrame,
    data_type: FileDataType,
    output_file_prefix: str = DEFAULT_PLOT_PREFIX,
    show_in_console: bool = False,
) -> None:
    """Barplot of compression per time unit [MB of data reduction / minute]"""

    # Subset input data frame to keep only the rows needed.
    df = input_df.loc[input_df["data_type"] == data_type.value, :].copy()

    # Fix values of rows where output_size > data_size. This happens when there is
    # no compression, since the sett packaging will add a small overhead to the
    # total data size.
    mask = df["output_size"] > df["data_size"]
    df.loc[mask, "output_size"] = df.loc[mask, "data_size"]

    # Compute the compression rate: the amount of data size reduction per time
    # unit, here MB of data reduction / minute.
    df["compression_rate"] = (df["data_size"] - df["output_size"]) / (
        df["encrypt_time_mean"]
    )

    # Create plot figure.
    sns.set(style="whitegrid")
    with sns.plotting_context("notebook", font_scale=1.4):
        g = sns.catplot(
            x="compression_level",
            y="compression_rate",
            data=df,
            hue="compression_cmd",
            kind="bar",
            height=12,
            palette=compression_cmds_colors(tuple(df["compression_cmd"].unique())),
            legend_out=False,
        )
        g.despine(left=True)

    # Add axis labels, title, subtitle and legend to plot.
    plt.subplots_adjust(top=0.85, left=0.08, bottom=0.08, right=0.95)
    g.ax.set_xlabel("gzip compression level", size=15, labelpad=12)
    g.ax.set_ylabel("compression rate [MB data reduction / sec]", size=15, labelpad=12)
    plt.title("Compression rate vs. compression level for text data", size=18, y=1.15)
    plt.suptitle(data_summary_str(df), y=0.96, x=0.5, ha="center", size=14)
    add_legend(ax=g.ax, bbox_to_anchor=(1, 1))

    # Save figure to disk.
    g.savefig(f"{output_file_prefix}_compression_rate_vs_compression_level.png")
    if show_in_console:
        plt.show()


SUPPORTED_PRESETS = ("short", "full")


def cli_argument_parser() -> argparse.Namespace:
    """User input options for the CLI."""

    parser = argparse.ArgumentParser(
        prog="plot_benchmarks.py",
        usage="%(prog)s [options]",
        description=f"Plot {APP_NAME_SHORT} benchmark results for "
        "'short' and 'full' presets",
        formatter_class=argparse.RawTextHelpFormatter,
    )

    for preset_name in SUPPORTED_PRESETS:
        parser.add_argument(
            f"-{preset_name[0]}",
            f"--{preset_name}-result-file",
            action="store",
            dest=f"input_file_{preset_name}",
            default=f"benchmark_results_{preset_name}.tsv",
            type=str,
            help=(f"benchmark result file for preset '{preset_name}'"),
        )

    parser.add_argument(
        "-r",
        "--root-dir",
        action="store",
        dest="root_dir",
        default=load_benchmark_config().root_dir,
        type=str,
        help=("Directory where to look for input files and save outputs"),
    )

    return parser.parse_args()


if __name__ == "__main__":
    plot_in_console = False

    # Logger setup.
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter("%(message)s"))
    logger.addHandler(handler)

    logging.info(
        "Benchmark results plotting script started\n"
        "Retrieving benchmark root directory...\n"
    )

    # Get input files and root directory.
    args = cli_argument_parser()
    output_prefix = os.path.join(args.root_dir, DEFAULT_PLOT_PREFIX)
    results_df = {}
    for preset in SUPPORTED_PRESETS:
        file_path = os.path.join(args.root_dir, getattr(args, f"input_file_{preset}"))
        logging.info("Loading data from file: %s", file_path)
        if os.path.isfile(file_path):
            results_df[preset] = load_benchmark_results_file(file_path)
            log_data_summary(input_df=results_df[preset], file_name=file_path)
        else:
            logging.warning(
                "WARNING: unable to find file [%s]. Some plots will not be generated",
                file_path,
            )

    # Check that all input files use same sett version.
    sett_versions = {
        get_df_column_unique_value(df, "sett_version") for df in results_df.values()
    }
    if len(sett_versions) > 1:
        logger.warning("WARNING: sett version differs between input files.")

    # Generate plots:
    if "short" in results_df:
        # Sett task time as a function of input data size.
        # This plot is drawn using the "short" benchmark results. These are run
        # for data sizes between 100 MB and 100 GB, but only with a single
        # compression level (4), a single data type (text) and file size type
        # (mixed). These choices were made because:
        #  -> compression level 4 is the optimal value for sett.
        #  -> the "mixed" file_size_type, because this factor has no impact on
        #     the outcome.
        lineplot_total_time_by_data_size(
            input_df=results_df["short"],
            data_type=FileDataType.TEXT,
            file_size_type=FileSizeType.MIXED,
            compression_level=4,
            output_file_prefix=output_prefix,
            show_in_console=plot_in_console,
        )

    if "full" in results_df:
        # Total time (packaging + transfer + decryption) as a function of
        # compression level. We only draw the plot for an input data of 1 GB
        # text, because when doing it for other data_sizes and file_size_types,
        # the shape of the plot is the same.
        lineplot_total_time_by_compression_level(
            input_df=results_df["full"],
            data_size=1000,
            data_type=FileDataType.TEXT,
            file_size_type=FileSizeType.MIXED,
            output_file_prefix=output_prefix,
            show_in_console=plot_in_console,
        )

        # Output size as function of compression level.
        barplot_sett_output_size_by_compression_level(
            input_df=results_df["full"],
            data_type=FileDataType.TEXT,
            output_file_prefix=output_prefix,
            show_in_console=plot_in_console,
        )

        # Data compression rate, i.e. MB of data compression per second.
        barplot_compression_per_time_unit(
            input_df=results_df["full"],
            data_type=FileDataType.TEXT,
            output_file_prefix=output_prefix,
            show_in_console=plot_in_console,
        )

    logging.info("Benchmark results plotting script completed successfully")
