# sett benchmark

For detailed instructions, please refer to the following documentation
<https://sett.readthedocs.io/en/stable/benchmarks.html>.

## The (very) quick-start guide

Download the `run_benchmarks.py` and run it via the following commands.

```bash
./run_benchmarks.py                     # run the benchmarks.
./run_benchmarks.py --dry-run           # run the script in "dry-run" mode.
./run_benchmarks.py -d --preset=full    # dry-run mode with "full" preset.
./run_benchmarks.py -d --preset=short   # dry-run mode with "short" preset.
./run_benchmarks.py --preset=quick      # run the benchmarks with "quick" preset.
```

The benchmark outputs are saved in a tab-delimited file named
`benchmark_results.tsv` (or `benchmark_results_<preset>.tsv` if running a
preset), which is saved in the directory specified through the `root_dir`
parameter.

Benchmark parameters can be modified via a config file located at:

- Linux: `~/.config/sett/benchmark_config.json`
- Windows: `~\AppData\Roaming\sett\benchmark_config.json`
- Mac OS: `~/.config/sett/benchmark_config.json`

## Plotting results

The `run_benchmarks.py` script has a companion script named `plot_benchmarks.py`
that allows plotting the output results after having run the `short` and `full`
presets. Note that plotting requires the python modules `pandas`, `matplotlib`
and `seaborn` to be installed.

**IMPORTANT:** the `plot_benchmarks.py` script only works to plot the results
from the `short` and `full` presets. By default the input files are expected
to be named `benchmark_results_short.tsv` and `benchmark_results_full.tsv`,
but custom names can be passed via the `-s` and `-f` arguments. Please run
`plot_benchmarks.py --help` for more details.

The set of commands to run is the following:

```bash
./run_benchmarks.py --preset=short
./run_benchmarks.py --preset=full
./plot_benchmarks.py
```

## Requirements

- [python](https://www.python.org) >= 3.8
- [podman](https://podman.io) if the `sftp_server` argument value is set
  to `podman`.
- [docker](https://www.docker.com) if the `sftp_server` argument value is set
  to `docker`.
- `pigz` if using external compression as part of the benchmarks (see
  `compression_cmd` argument in the config file).
- If using the `plot_benchmarks.py` script, the python modules `pandas`,
  `matplotlib` and `seaborn` must be installed.
- **WARNING:** the benchmarks currently **only work on Linux and MacOS X systems**.
