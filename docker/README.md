# Docker container for sett CLI

This README contains the instructions to build and run a Docker container with
`sett`.

Please note that this container **only allows to run `sett` in CLI mode**
(command line interface), there is no support for the graphical user interface
(GUI).

## Run the container

When running the container, please pay attention to the following:

* The directory containing the data to encrypt must be mounted as a volume
  mapped to `/data` inside the container.
  This is done through the option (see also example below):

    ```sh
    -v <path to directory with data to encrypt>:/data
    ```

    For instance, if the data to encrypt is located under `/home/alice/data`,
    then the option to add is `-v /home/alice/data:/data`.

* The directory containing your PGP keys must be mounted inside the container.
  On a Linux machine, this directory is usually `~/.gnupg` and the option to
  mount it inside the container is thus the following (see also example below):

  ```sh
  -v ~/.gnupg:/root/.gnupg
  ```

* The encrypted output file should be saved to `/data` inside the container,
  so that it will then be available *outside* the container on your local
  machine (remember that we are mounting a local directory to `/data`). This
  is done by passing `--output /data` to `sett` (see also example below).

Here is an example of how to encrypt data with `sett` running in the container.
Remember to modify the following things:

* Replace `/home/alice/data` with the directory containing the data to encrypt.
* Set the correct `--sender` and `--recipient` values.
* Set the correct `--dtr-id` value.
* Check that you are using the latest version (or the desired version) of
  `sett` by specifying the correct version number in the container tag. In
  the example below, the container that is used is
  `registry.gitlab.com/biomedit/sett:4.3.0`, i.e. the version `4.3.0` of `sett`.
  To use a different version, replace `4.3.0` with the desired version.

```sh
# Example of data encryption.
docker run -i --rm \
    -v /home/alice/data:/data \
    -v ~/.gnupg:/root/.gnupg \
    registry.gitlab.com/biomedit/sett:4.3.0 encrypt \
    --sender alice.sender@example.com \
    --recipient bob.recipient@example.com \
    --output-suffix=test_landingzone \
    --dtr-id=<DTR ID number> --purpose=TEST \
    --output /data \
    /data/<file or directory to encrypt>
```

```sh
# To check whether your container works, try displaying the sett version.
docker run --rm registry.gitlab.com/biomedit/sett:4.3.0 --version
```

## Build the container locally

By default, the latest versions of `sett` available on PyPI will be installed
in the container. Older versions of `sett` can be installed by passing the
argument `--build-arg=VERSION=<version to install>` to the `docker build`
command, see the examples below.

```sh
docker build --tag=sett:latest .                           # Build container with latest version.
docker build --tag=sett:4.1.0 --build-arg=VERSION=4.1.0 .  # Build container with a specific version.
```

Building a container with an un-released version (i.e. a version that was not
yet released on PyPI) is also possible. To do so, simply pass the un-released
version number to the `docker build` command via
`--build-arg=VERSION=<un-released version`.
This will build the container using the latest version of the sett source code
downloaded from its GitLab repo.

In this example, the un-released version is `4.3.0` (which did not exist on
PyPI at the time this example was made).

```sh
docker build --tag=sett:4.3.0 --build-arg=VERSION=4.3.0 .
docker run --rm sett:4.3.0 --version
```
