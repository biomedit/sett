# Contributing to sett

## Development Setup

### Installation

After cloning the repository install the package in editable mode - this allows
trying changes without reinstalling the package.

```bash
pip install -e .
```

Install development dependencies.

```bash
pip install pre-commit bandit mypy pylint tox pytest
```

### Code style

This project uses [black](https://github.com/psf/black) for code formatting.
For convenience, you can install a pre-commit hook for black. For pre-commit
installation instruction refer to [documentation](https://pre-commit.com/#install)).

```bash
pre-commit install
```

In addition, [pylint](https://pylint.org/), [mypy](https://mypy.readthedocs.io)
and [bandit](https://bandit.readthedocs.io) are used static code analysis.

```bash
bandit -r sett/
mypy --strict sett/ test/
pylint sett/ test/
```

### Testing

Running unit tests locally.

- Install `tox`.
- Run `tox` to execute tests with the default configuration.
- Customize test runtime, for example:

  - Run tests from a specific file/directory in the Python 3.9 environment

    ```bash
    tox -e py39 -- test/core
    ```

  - Filter by name to only run specific tests

    ```bash
    tox -e py39 -- -k TestFilesystem
    # Name filter can also be combined with path
    tox -e py39 -- -k TestFilesystem test/core
    ```

  Note: arguments passed after `--` are passed directly to `pytest` not `tox`.

## Commit Message Guidelines

This project follows the [Angular commit message guidelines](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines)
for its commit messages.

### Template

Fields shown in `[square brackets]` are optional.

```text
type[(scope)]: subject line - max 100 characters

[body] - extended description of commit that can stretch over
multiple lines. Max 100 character per line.

[footer] - links to issues with (Closes #, Fixes #, Relates #) and BREAKING CHANGE:
```

#### Type

The following types are allowed. Only commits with types shown in **bold** are
automatically added to the changelog, as well as those containing the
`BREAKING CHANGE:` keyword):

- **feat**: new feature.
- **fix**: bug fix.
- build: changes that affect the build system or external dependencies.
- ci: changes to CI configuration files and scripts.
- docs: documentation only changes.
- perf: code change that improves performance.
- refactor: code change that neither fixes a bug nor adds a feature.
- style: change in code formatting only (no impact on functionality).
- test: change in unittest files only.

#### Scope

- Name of the file/functionality/workflow affected by the commit.

#### Subject line

- One line description of commit with max 100 characters.
- Use the imperative form, e.g. "add new feature" instead of "adds new feature"
  or "added a new feature".
- No "." at the end of the subject line.

#### Body

- Extended description of commit that can stretch over multiple lines.
- Max 100 characters per line.
- Explain things such as what problem the commit addresses, background info on
  why the change was made, or alternative implementations that were tested but
  didn't work out.

#### Footer

- Reference to git issues with `Closes/Close #x`, `Fixes/Fix #x`, `Related #x`.
- Location for the `BREAKING CHANGE:` keyword. If the commit introduces a
  breaking change in the code, add this keyword followed by a description of
  what the commit breaks, why it was necessary, and how users should port their
  code to adapt to the new version.
  Any commit containing the pattern `BREAKING CHANGE:` will be added to the
  changelog, regardless of its type.

### Commit messages and auto-versioning

The following rules are applied by the auto-versioning system to modify the
version number when a new commit is pushed to the `master`/`main` branch:

- Keyword `BREAKING CHANGE:`: increases the new major digit, e.g. 1.1.1 -> 2.0.0
- Type `feat`: increases the minor version digit, e.g. 1.1.1 -> 1.2.0
- Type `fix`: increases the patch digit, e.g. 1.1.1 -> 1.1.2

**Note:** an exception to the behavior of `BREAKING CHANGE:` is for pre-release
versions (i.e. 0.x.x). In that case, a `BREAKING CHANGE:` keyword increases the
minor digit rather than the major digit. For more details, see the
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) and the
[semantic versioning](https://semver.org/spec/v2.0.0.html) specifications.

### Examples

```text
feat(decrypt): add public key auto-download

Allows sett to auto-download public keys from the keyserver when encrypting/decrypting data
for/from recipients/senders whose key is not available in the user's local keyring.
This feature was added to make the life of users easier, so the encryption and decryption workflows
do not crash when a key is missing.

BREAKING CHANGE: config file was changed with the addition of a new "public_key_auto_download"
    option that enables auto-downloading of public keys from the default keyserver when set to
    'True'.

    To migrate a config file, follow the example below:

        Before:

        config: {
          gpg_dir: '~/.gnupg',
          certicifation_key: 'fingerprint',
        }

        After:

        config: {
          gpg_dir: '~/.gnupg',
          certicifation_key: 'fingerprint',
          allow_key_autodownload: 'False',
        }

Closes #7, #123, Related #23
```

```text
fix(gui): make warning message more explicit when key is not signed

Improves the readability of the message displayed to the user when a key is missing the
signature from the central authority.
The message was changed from:
    "Only keys signed by <key fingerprint> are allowed to be used."
to:
    "Only keys signed by <key user ID> <key fingerprint> are allowed to be used. Please get your
     public key signed by <key user ID>."

Closes #141
```

```text
docs(CONTRIBUTING): add examples of conventional commit messages

Add a few examples of conventional commit messages to CONTRIBUTING.md, so that people don't have
to click on the "Angular commit message guidelines" link to read the full specifications.
Add a concise summary of the guidelines to provider a reminder of the main types and keywords.

Closes #114, #139
```

### Setting up the commit message template

To have Git automatically a commit message template when running `git commit`,
either run the following command at the root of the git repository:

```shell
git config commit.template .gitmessage
```

Or manually add the following block to `.git/config`:

```text
[commit]
  template = .gitmessage
```

*Note:* to have this configuration applied user-wide, add the `--global` option
to the `git config` command above or add the above-mentioned block to the
`~/.gitconfig` file. Be aware that any repository specific configuration will
still override any user level - or system level - defined configuration.

### Using `commitizen` to enforce commit message compliance

To enforce compliance with the commit message template a pre-commit hook for
commitizen can be installed (for pre-commit installation instruction refer to
[the documentation](https://pre-commit.com/#install)).

```bash
pre-commit install --hook-type commit-msg
```

The hook can be uninstalled with `pre-commit uninstall --hook-type commit-msg`.

The commitizen interactive tool can also be used to write the commit message.
This requires commitizen to be installed locally.

```bash
pip install commitizen
```

Then, instead of running `git commit`, run `cz commit`. This will prompt a
series of questions, and create a template-compliant commit message.
