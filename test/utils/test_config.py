import io
import json
import os
import unittest
from pathlib import Path
from typing import Dict, Tuple, Union, Any
from unittest.mock import patch, Mock, mock_open
from contextlib import nullcontext as does_not_raise
import pytest
from libbiomedit.lib.deserialize import serialize, deserialize

from sett.core.error import UserError
from sett.core.filesystem import reverse_expanduser
from sett.core.secret import Secret
from sett.protocols import sftp
from sett.utils import config
from sett.utils import get_config_path
from sett.utils.config import (
    Config,
    ConnectionStore,
    config_to_dict,
    default_config,
)
from sett.utils.get_config_path import (
    CONFIG_FILE_ENVIRON_VAR,
    CONFIG_FILE_NAME,
    get_config_file,
)


class TestConfig(unittest.TestCase):
    @staticmethod
    @patch("sett.utils.config.sys_config_dict", lambda: {})
    def test_default_config_serializable() -> None:
        json.dumps(config_to_dict(config.default_config()))

    def test_load_config(self) -> None:
        home_dir = os.path.expanduser("~")
        default_gpg_path = os.path.join(home_dir, ".gnupg")
        custom_gpg_path = os.path.join(home_dir, "custom", "path", ".gnupg")
        repo_url = "https://pypi.org"

        # Test loading data from a system-wide config file.
        #  * values from the config should override default values.
        #  * when loading URL with a trailing "/", that "/" should be stripped.
        #  * home dir shortcuts ("~") in paths should be expanded.
        sys_config_contents: Tuple[Dict[str, Union[str, bool]], ...] = (
            {},
            {
                "repo_url": repo_url,
                "gpg_home_dir": default_gpg_path,
            },
            {
                "repo_url": repo_url + "/",
                "gpg_home_dir": os.path.join("~", ".gnupg"),
                "gui_quit_confirmation": False,
            },
        )
        for sys_config_content in sys_config_contents:
            with patch(
                "sett.utils.config.sys_config_dict", lambda: sys_config_content
            ), patch("sett.utils.config.load_config_dict", lambda _: {}):
                cfg = config.load_config()
                self.assertEqual(cfg.default_sender, None)
                if "repo_url" in sys_config_content:
                    self.assertEqual(cfg.repo_url, repo_url)
                if "gpg_home_dir" in sys_config_content:
                    self.assertEqual(cfg.gpg_home_dir, default_gpg_path)
                if "gui_quit_confirmation" in sys_config_content:
                    self.assertEqual(cfg.gui_quit_confirmation, False)
                else:
                    self.assertEqual(cfg.gui_quit_confirmation, True)

        # Test loading data from a user config file.
        #  * values from the user config should override values from the
        #    system-wide config.
        #  * when loading URL with a trailing "/", that "/" should be stripped.
        #  * home dir shortcuts ("~") in paths should be expanded.
        sys_config_content = {
            "repo_url": repo_url,
            "gpg_home_dir": os.path.join("~", ".gnupg"),
            "gui_quit_confirmation": True,
        }
        user_config_contents: Tuple[Dict[str, Union[str, bool]], ...] = (
            {
                "default_sender": "A" * 40,
                "log_dir": os.path.join("~", ".local", "log"),
            },
            {
                "default_sender": "A" * 40,
                "log_dir": os.path.join("~", ".local", "log"),
                "repo_url": "https://alternative.pypi.org/",
                "gpg_home_dir": os.path.join("~", "custom", "path", ".gnupg"),
                "gui_quit_confirmation": False,
            },
        )
        for user_config_content in user_config_contents:
            with patch(
                "sett.utils.config.sys_config_dict", lambda: sys_config_content
            ), patch(
                "sett.utils.config.load_config_dict",
                lambda _: user_config_content,  # pylint: disable=cell-var-from-loop
            ):
                cfg = config.load_config()
                self.assertEqual(cfg.default_sender, "A" * 40)
                self.assertEqual(cfg.log_dir, os.path.join(home_dir, ".local", "log"))

                if "repo_url" in user_config_content:
                    self.assertEqual(cfg.repo_url, "https://alternative.pypi.org")
                else:
                    self.assertEqual(cfg.repo_url, repo_url)

                if "gpg_home_dir" in user_config_content:
                    self.assertEqual(cfg.gpg_home_dir, custom_gpg_path)
                else:
                    self.assertEqual(cfg.gpg_home_dir, default_gpg_path)

                if "gui_quit_confirmation" in user_config_content:
                    self.assertEqual(cfg.gui_quit_confirmation, False)
                else:
                    self.assertEqual(cfg.gui_quit_confirmation, True)

    # get_config_file() tests:
    # Case 1: when the config file path environmental variable is not defined,
    # the function should return the default config file location.
    def test_get_config_file_from_default_path(self) -> None:
        config_path = get_config_file()
        default_path = os.path.join(get_config_path.get_config_dir(), CONFIG_FILE_NAME)
        self.assertEqual(config_path, default_path)

    # Case 2: when a custom location is passed via the appropriate
    # environmental variable, get_config_file() should now return that
    # custom location.
    @patch.dict(
        os.environ,
        {CONFIG_FILE_ENVIRON_VAR: "~/fake/path/sett_config.json"},
    )
    def test_get_config_file_from_environ(self) -> None:
        config_path = get_config_file()
        self.assertEqual(config_path, os.environ.get(CONFIG_FILE_ENVIRON_VAR))

    # Case 3: a custom location is passed via the environmental variable,
    # but it points to a directory rather than a file. This should raise
    # an error.
    @patch.dict(os.environ, {CONFIG_FILE_ENVIRON_VAR: "~/fake/directory"})
    @patch("os.path.isdir", return_value=True)
    def test_get_config_file_from_environ_bad_input(self, mock_isdir: Mock) -> None:
        self.assertRaises(UserError, get_config_file)
        mock_isdir.assert_called_once_with(os.environ.get(CONFIG_FILE_ENVIRON_VAR))

    def test_reverse_expanduser(self) -> None:
        home_dir = Path.home()
        path_sep = os.sep
        home_dir_split = home_dir.as_posix().strip("/").split("/")
        not_home_dir_path = os.path.join("not", "a", "home", "dir", "path")
        bad_home_dir_path = os.path.join("a", "bad", *home_dir_split, "path")

        # Note on test paths and their expected values:
        # * Paths 1 and 2 start with a proper home directory, which should
        #   be replaced by "~".
        # * Path 3 is missing the leading separator. It should not be modified.
        # * The remaining path do not start in the home directory. Their values
        #   should not be modified except for the removal of trailing
        #   separators.
        test_paths = (
            (
                path_sep + os.path.join(*home_dir_split, "some", "path"),
                os.path.join("~", "some", "path"),
            ),
            (
                path_sep
                + os.path.join(*home_dir_split, "some", "relative", "..", "path"),
                os.path.join("~", "some", "relative", "..", "path"),
            ),
            (
                os.path.join(*home_dir_split, "some", "path"),
                os.path.join(*home_dir_split, "some", "path"),
            ),
            (not_home_dir_path, not_home_dir_path),
            (path_sep + not_home_dir_path, path_sep + not_home_dir_path),
            (path_sep + not_home_dir_path + path_sep, path_sep + not_home_dir_path),
            (bad_home_dir_path, bad_home_dir_path),
        )
        for input_path, expected_path in test_paths:
            self.assertEqual(reverse_expanduser(input_path), expected_path)

    def test_config_to_dict(self) -> None:
        # Verify that paths values that point to the user's home directory
        # are replaced with the "~" shortcut.
        settings_that_are_path = ("gpg_home_dir", "log_dir", "output_dir")
        test_config = Config()
        home_dir = os.path.expanduser("~")

        # Set arbitrary paths located in the home directory, to see if they
        # are properly converted to "~" during config file writing.
        for setting in settings_that_are_path:
            setattr(test_config, setting, os.path.join(home_dir, "subdir", setting))

        # Convert to dict, then check that the serialization has converted
        # home dir path to "~".
        config_as_dict = config_to_dict(test_config)
        for setting in settings_that_are_path:
            self.assertEqual(
                config_as_dict[setting], os.path.join("~", "subdir", setting)
            )

    def test_save_config(self) -> None:
        # Verify that the config dict passed to the function is written to
        # to correct file path.
        config_dict = config_to_dict(Config())
        config_dict_expected = config_dict.copy()

        test_config_path = "/test/path/config.json"
        with patch("builtins.open", new_callable=mock_open()) as m, patch(
            "json.dump"
        ) as m_json, patch("pathlib.Path.is_dir", lambda _: True):
            config.save_config(config=config_dict, path=test_config_path)

        # Note: ".__enter__.return_value" is here needed because the call to
        # builtin open() is made inside a context manager.
        m_json.assert_called_once_with(
            config_dict_expected,
            m.return_value.__enter__.return_value,
            indent=2,
            sort_keys=True,
        )
        m.assert_called_once_with(Path(test_config_path), "w", encoding="utf-8")


connection_store_type = Dict[str, Dict[str, Dict[str, Dict[str, str]]]]


class InMemoryConnectionStore(ConnectionStore):
    def __init__(self, cfg: Config):
        super().__init__("")
        self._f = io.StringIO(json.dumps(config_to_dict(cfg)))

    def _read(self) -> connection_store_type:
        return_value: connection_store_type = json.loads(self._f.getvalue())
        return return_value

    def _write(self, data: connection_store_type) -> None:
        """Write data to config file"""
        self._f.truncate(0)
        self._f.write(json.dumps(data))

    def get_dict(self) -> connection_store_type:
        return self._read()


pkey = os.path.join(Path.home(), ".ssh", "id_rsa")
reversed_pkey = os.path.join("~", ".ssh", "id_rsa")
params = {
    "username": "chuck",
    "host": "localhost",
    "pkey_password": "123",
    "pkey": pkey,
}
conn = sftp.Protocol(
    username="chuck",
    host="localhost",
    pkey_password=Secret("123"),
    pkey=pkey,
    destination_dir=".",
)


class TestConnectionStore(unittest.TestCase):
    def setUp(self) -> None:
        self.config = Config()
        self.conn1 = sftp.Protocol(
            username="",
            host="",
            destination_dir=".",
        )
        self.config.connections["conn1"] = self.conn1
        self.connection_store = InMemoryConnectionStore(self.config)

    def test_rename(self) -> None:
        self.connection_store.rename("conn1", "new_conn")
        self.config.connections["new_conn"] = self.config.connections.pop("conn1")
        connection_store = self.connection_store.get_dict()
        self.assertEqual(connection_store, config_to_dict(self.config))
        self.assertNotIn("conn1", connection_store["connections"])
        with self.assertRaises(UserError):
            self.connection_store.rename("foo", "bar")

    def test_delete(self) -> None:
        self.connection_store.delete("conn1")
        connection_store = self.connection_store.get_dict()
        self.assertEqual(connection_store["connections"], {})
        with self.assertRaises(UserError):
            self.connection_store.delete("foo")

    def test_save(self) -> None:
        new_label = "new connection"
        self.connection_store.save(new_label, conn)
        connection_store = self.connection_store.get_dict()
        self.assertEqual(
            sorted(connection_store["connections"].keys()), sorted(["conn1", new_label])
        )
        self.assertEqual(
            connection_store["connections"][new_label]["parameters"]["pkey_password"],
            None,
        )
        self.assertEqual(
            connection_store["connections"][new_label]["parameters"]["pkey"],
            reversed_pkey,
        )


class TestConnectionSerialization(unittest.TestCase):
    @patch("sett.utils.config.sys_config_dict", lambda: {})
    def test_serialize(self) -> None:
        cfg: Config = default_config()
        cfg.connections["conn"] = conn
        serialized = serialize(Config)(cfg)
        self.assertEqual(
            serialized["connections"]["conn"]["parameters"]["pkey"], reversed_pkey
        )

    def test_deserialize(self) -> None:
        serialized = {
            "dcc_portal_url": "https://portal.dcc.sib.swiss",
            "keyserver_url": "https://keys.openpgp.org/",
            "gpg_home_dir": "~/.gnupg",
            "always_trust_recipient_key": True,
            "repo_url": "https://pypi.org",
            "check_version": True,
            "log_dir": "~/.local/var/log/sett",
            "error_reports": True,
            "log_max_file_number": 1000,
            "connections": {
                "conn": {
                    "protocol": "sftp",
                    "parameters": {
                        "username": "chuck",
                        "host": "localhost",
                        "pkey_password": "123",
                        "destination_dir": ".",
                        "pkey": "~/.ssh/id_rsa",
                    },
                }
            },
            "output_dir": None,
            "ssh_password_encoding": "utf_8",
            "default_sender": None,
            "gui_quit_confirmation": True,
            "compression_level": 5,
            "package_name_suffix": None,
            "max_cpu": 0,
            "gpg_key_autodownload": True,
            "verify_package_name": True,
        }
        deserialized: Config = deserialize(Config)(serialized)
        self.assertEqual(getattr(deserialized.connections["conn"], "pkey"), pkey)


@pytest.mark.parametrize(
    "config_key, is_mandatory, expectation",
    [
        # Test that should pass:
        #  * A mandatory field should be evaluated as mandatory.
        ["keyserver_url", False, does_not_raise()],
        ["output_dir", False, does_not_raise()],
        ["default_sender", False, does_not_raise()],
        #  * An optional field should be evaluated as non-mandatory.
        ["dcc_portal_url", True, does_not_raise()],
        ["max_cpu", True, does_not_raise()],
        ["verify_package_name", True, does_not_raise()],
        #
        # Test that should raise an error:
        #  * A field that does not exist in the config should raise an error.
        [
            "allow_roundhouse_kicks_on_user_error",
            False,
            pytest.raises(
                ValueError,
                match="has no attribute 'allow_roundhouse_kicks_on_user_error'",
            ),
        ],
    ],
)
def test_is_mandatory_argument(
    config_key: str, is_mandatory: bool, expectation: Any
) -> None:
    """Test that mandatory and non-mandatory fields are detected correctly."""
    with expectation:
        test_value = Config.is_mandatory_argument(arg=config_key)
        assert test_value == is_mandatory


@pytest.mark.parametrize(
    "config_key, actual_default_value",
    [
        # Test that should pass:
        #  * The requested key (config argument) has a default value.
        ["keyserver_url", Config().keyserver_url],
        ["output_dir", Config().output_dir],
        ["max_cpu", Config().max_cpu],
    ],
)
def test_get_default_value(config_key: str, actual_default_value: str) -> None:
    """Test that the default value of a field is retrieved correctly."""

    assert Config.get_default_value(arg=config_key) == actual_default_value


@pytest.mark.parametrize(
    "config_key, actual_label",
    [
        # Test that should pass:
        #  * The requested key (config argument) has a default value.
        ["keyserver_url", "Keyserver URL"],
        ["output_dir", "Output directory"],
        ["max_cpu", "Max CPU"],
    ],
)
def test_get_label(config_key: str, actual_label: str) -> None:
    """Test that the default value of a field is retrieved correctly."""

    assert Config.get_label(arg=config_key) == actual_label
