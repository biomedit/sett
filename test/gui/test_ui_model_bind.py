import abc
import unittest
from unittest import mock
from pathlib import Path
from typing import TypeVar, Generic, Any, Type, Optional

from sett.core.secret import Secret
from sett.gui.ui_model_bind import (
    bind,
    BoolControl,
    NumericControl,
    TextControl,
    OptionalTextControl,
    PasswordControl,
    OptionalPasswordControl,
    PathControl,
    OptionalPathControl,
    Control,
)
from sett.gui.listener import ClassWithListener
from sett.gui.pyside import QtCore

T = TypeVar("T")


class MockListener(ClassWithListener, Generic[T]):
    def __init__(self, val: T):
        super().__init__()
        self.attr = val


class _ExcludeFromTestsNamespace:
    class TestModelBindBase(unittest.TestCase):
        initial_value: Any
        final_value: Any
        ControlType: Type[Control[Any, Any]]
        change_from_ui: Any
        expected_ui_value: Any

        @staticmethod
        @abc.abstractmethod
        def _get_widget_signal(widget: mock.Mock) -> mock.Mock:
            """Define how to get the signal that is supposed to be connected."""

        @staticmethod
        @abc.abstractmethod
        def _get_widget_setter(widget: mock.Mock) -> mock.Mock:
            """Define how to get the setter that is supposed to be triggered
            after binding.
            """

        def setUp(self) -> None:
            self.widget = mock.Mock()
            self.listener = MockListener(self.initial_value)

            bind(self.listener, "attr", self.widget, self.ControlType)

        def _get_callback(self) -> Any:
            return self._get_widget_signal(self.widget).connect.call_args.args[0]

        def test_that_bind_works_from_widget_to_listener(self) -> None:
            self.listener.attr = self.final_value
            self._get_widget_setter(self.widget).assert_called_once_with(
                self.expected_ui_value
            )

        def _trigger_ui_change(self) -> None:
            """Emulate a UI change"""
            self._get_callback()(self.change_from_ui)

        def test_that_bind_works_from_listener_to_widget(self) -> None:
            self.assertEqual(self.listener.attr, self.initial_value)
            self._trigger_ui_change()
            self.assertEqual(self.listener.attr, self.final_value)


class TestUiModelBindBool(_ExcludeFromTestsNamespace.TestModelBindBase):
    initial_value = True
    final_value = False
    ControlType = BoolControl
    change_from_ui = QtCore.Qt.CheckState.Unchecked
    expected_ui_value = final_value

    @staticmethod
    def _get_widget_signal(widget: mock.Mock) -> Any:
        return widget.stateChanged

    @staticmethod
    def _get_widget_setter(widget: mock.Mock) -> Any:
        return widget.setChecked


class TestUiModelBindNumeric(_ExcludeFromTestsNamespace.TestModelBindBase):
    initial_value = 1
    final_value = 2
    ControlType = NumericControl
    change_from_ui = final_value
    expected_ui_value = final_value

    @staticmethod
    def _get_widget_signal(widget: mock.Mock) -> Any:
        return widget.valueChanged

    @staticmethod
    def _get_widget_setter(widget: mock.Mock) -> Any:
        return widget.setValue


class TestUiModelBindText(_ExcludeFromTestsNamespace.TestModelBindBase):
    initial_value: Any = "A"
    final_value: Any = "B"
    ControlType = TextControl[Optional[str]]
    change_from_ui = final_value
    expected_ui_value = final_value

    def _trigger_ui_change(self) -> None:
        """Emulate a UI change"""
        self.widget.text = mock.Mock(return_value=self.change_from_ui)
        self._get_callback()()

    @staticmethod
    def _get_widget_signal(widget: mock.Mock) -> Any:
        return widget.editingFinished

    @staticmethod
    def _get_widget_setter(widget: mock.Mock) -> Any:
        return widget.setText


class TestUiModelBindOptionalText(TestUiModelBindText):
    initial_value = "A"
    final_value = None
    ControlType = OptionalTextControl
    change_from_ui = ""
    expected_ui_value = ""


class TestUiModelBindSecret(TestUiModelBindText):
    initial_value = Secret("A")
    final_value = Secret("B")
    ControlType = PasswordControl
    change_from_ui = "B"
    expected_ui_value = "B"


class TestUiModelBindOptionalSecret(TestUiModelBindText):
    initial_value = Secret("A")
    final_value = None
    ControlType = OptionalPasswordControl
    change_from_ui = ""
    expected_ui_value = ""


class TestUiModelBindPath(_ExcludeFromTestsNamespace.TestModelBindBase):
    initial_value = "/tmp"
    final_value: Optional[str] = "/var"
    ControlType = PathControl
    change_from_ui: Optional[str] = final_value
    expected_ui_value: Optional[Path] = Path("/var")

    def _get_callback(self) -> Any:
        return self.widget.on_path_change.call_args.args[0]

    @staticmethod
    def _get_widget_signal(widget: mock.Mock) -> Any:
        return widget.textChanged

    @staticmethod
    def _get_widget_setter(widget: mock.Mock) -> Any:
        return widget.update_path


class TestUiModelBindOptionalPath(TestUiModelBindPath):
    initial_value = "/tmp"
    final_value = None
    ControlType = OptionalPathControl
    change_from_ui = None
    expected_ui_value = final_value
