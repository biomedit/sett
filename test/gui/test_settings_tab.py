import unittest
from collections import OrderedDict
from contextlib import ExitStack, contextmanager

# TODO: Remove unused-import when we can use Field unquoted (python>=3.8):
from dataclasses import dataclass, fields, Field  # pylint: disable=unused-import
from typing import (
    Any,
    Sequence,
    TypeVar,
    Optional,
    Union,
    Type,
    Dict,
    Iterator,
    Tuple,
    cast,
)
from unittest import mock

import pytest
from sett.gui.settings_tab import widget_row_from_field, align
from sett.utils.config import field_ext, FileType

T = TypeVar("T")


# TODO: when dropping support for python 3.8, remove quotes around "Field".
def prepare_field(t: Union[Type[T], object], *args: Any, **kwargs: Any) -> "Field[Any]":
    """Return a dummy field that looks like field from the Config class."""

    @dataclass
    class MockConfig:
        """Dummy Config-like class with a single field. The field must have
        the name of a real existing field of the original Config class.
        """

        keyserver_url: T = field_ext(*args, **kwargs)

    f = fields(MockConfig)[0]
    f.type = cast(type, t)
    return f


@contextmanager
def multipatch(
    *args: str, return_overrides: Optional[Dict[str, mock.Mock]] = None
) -> Iterator[Tuple[Tuple[mock.Mock, ...], Tuple[mock.Mock, ...]]]:
    """For given patch targets which possibly can occur multiple times,
    create patches.
    Each patch replaces a target with a factory.
    The patches are returned together with the factories return values.

    Example:
    ("A", "B", "A")
    will create two patches p_A for "A", and p_B for "B".
    The return value is ((p_A, p_B), (p_A(), p_B(), p_A()))
    """
    collected_patches: "OrderedDict[str, mock.Mock]" = OrderedDict(
        (target, mock.Mock(name=target + "()")) for target in args
    )
    if return_overrides:
        collected_patches.update(return_overrides)

    with ExitStack() as stack:
        yield (
            tuple(
                stack.enter_context(
                    mock.patch(
                        "sett.gui.settings_tab." + target,
                        mock.Mock(name=target, return_value=return_value),
                    )
                )
                for target, return_value in collected_patches.items()
            ),
            tuple(collected_patches[target] for target in args),
        )


# TODO: when dropping support for python 3.8, remove quotes around "Field".
class _ExcludeFromTestsNamespace:
    class TestWidgetFromField(unittest.TestCase):
        field: "Field[Any]"
        expected_widget_types: Tuple[str, ...]
        return_overrides: Optional[Dict[str, mock.Mock]] = None

        def setUp(self) -> None:
            self.mock_config = mock.Mock()
            self.register: Dict[str, Any] = {}

        def test_that_widget_row_from_field_creates_correct_widgets(self) -> None:
            with multipatch(
                *self.expected_widget_types, return_overrides=self.return_overrides
            ) as (
                mock_factories,
                expected_widget_instances,
            ):
                row = widget_row_from_field(self.mock_config, self.field, self.register)
                mock_label_factory, mock_widget_factory, *_ = mock_factories

            self.assertEqual(row, expected_widget_instances)
            mock_label_factory.assert_has_calls([mock.call("s")])
            expected_widget_instances[1].setStatusTip.assert_called_once_with("...")
            mock_widget_factory.assert_called_once()
            self.assertEqual(
                self.register, {self.field.name: expected_widget_instances[1]}
            )


class TestWidgetFromFieldStr(_ExcludeFromTestsNamespace.TestWidgetFromField):
    field = prepare_field(str, label="s", description="...")
    expected_widget_types = ("QtWidgets.QLabel", "LineEdit")


class TestWidgetFromFieldOptionalStr(_ExcludeFromTestsNamespace.TestWidgetFromField):
    field = prepare_field(Optional[str], label="s", description="...")
    expected_widget_types = ("QtWidgets.QLabel", "LineEdit")


def path_input_mock() -> mock.Mock:
    out = mock.Mock()
    out.text = out.btn = out.btn_clear = out
    return out


class TestWidgetFromFieldPath(_ExcludeFromTestsNamespace.TestWidgetFromField):
    field = prepare_field(
        str, label="s", description="...", file_type=FileType.directory
    )
    expected_widget_types = ("QtWidgets.QLabel", "PathInput", "PathInput", "PathInput")
    return_overrides = {"PathInput": path_input_mock()}


class TestWidgetFromFieldOptionalPath(_ExcludeFromTestsNamespace.TestWidgetFromField):
    field = prepare_field(
        Optional[str], label="s", description="...", file_type=FileType.directory
    )
    expected_widget_types = ("QtWidgets.QLabel", "PathInput", "PathInput", "PathInput")
    return_overrides = {"PathInput": path_input_mock()}


class TestWidgetFromFieldBool(_ExcludeFromTestsNamespace.TestWidgetFromField):
    field = prepare_field(bool, label="s", description="...")
    expected_widget_types = ("QtWidgets.QLabel", "QtWidgets.QCheckBox")


class TestWidgetFromFieldInt(_ExcludeFromTestsNamespace.TestWidgetFromField):
    field = prepare_field(int, label="s", description="...")
    expected_widget_types = ("QtWidgets.QLabel", "SpinBox")


class TestWidgetFromFieldIntWithBounds(_ExcludeFromTestsNamespace.TestWidgetFromField):
    field = prepare_field(int, label="s", description="...", minimum=1, maximum=4)
    expected_widget_types = (
        "QtWidgets.QLabel",
        "QtWidgets.QSlider",
        "QtWidgets.QLabel",
    )


@pytest.mark.parametrize(
    "table, expected",
    (
        (
            (((3, 42), (9,)), ((4, 5, 42), (9, "foo"))),
            ((3, 42, None, 9, None, None), (4, 5, 42, 9, "foo", None)),
        ),
        (
            (((3, 42, 8), ()),),
            ((3, 42, 8, None, None, None),),
        ),
        ((), ()),
    ),
)
def test_align(
    table: Sequence[Sequence[Sequence[Any]]],
    expected: Tuple[Tuple[Optional[Any], ...]],
) -> None:
    assert tuple(align(table)) == expected
