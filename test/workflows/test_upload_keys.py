import warnings
from typing import Callable, Any
from unittest.mock import Mock, patch

from test.factories import KeyFactory
import pytest
from gpg_lite.keyserver import VksEmailStatus
from pytest_factoryboy import register

from sett.core import gpg
from sett.core.crypt import KeyAlgorithm, verify_key_length
from sett.core.error import UserError
from sett.workflows.upload_keys import upload_keys
from . import DisableLogging, TestCaseWithRaiseError

register(KeyFactory, "rsa_key", pub_key_algorithm=KeyAlgorithm.RSA, key_length=3072)
register(KeyFactory, "dsa_key", pub_key_algorithm=KeyAlgorithm.DSA, key_length=4096)
register(KeyFactory, "ecc_key", pub_key_algorithm=KeyAlgorithm.ECC, key_length=255)


def test_verify_key_type_and_length(
    rsa_key: KeyFactory, dsa_key: KeyFactory, ecc_key: KeyFactory
) -> None:
    with pytest.raises(UserError):
        verify_key_length(rsa_key)
    with pytest.raises(UserError):
        verify_key_length(dsa_key)
    with warnings.catch_warnings(record=True) as w:
        # Cause all warnings to always be triggered.
        warnings.simplefilter("always")
        verify_key_length(ecc_key)
        assert len(w) == 1


class TestUploadKeys(TestCaseWithRaiseError):
    # Argument values.
    keyserver = "hagrid.hogwarts.wiz"
    key_fingerprints = ["A" * 40, "C" * 40]
    email = "chucknorris@roundhouse.gov"

    @classmethod
    def key_mock(cls) -> Callable[[Any, Any, Any], Mock]:
        return lambda k, c, sigs: Mock(
            key_id=k,
            fingerprint=k,
            uids=(gpg.model.Uid(full_name="Chuck Norris", email=cls.email),),
        )

    @patch("sett.workflows.upload_keys.upload_cert")
    @patch("sett.workflows.upload_keys.crypt_request_key_verification")
    @patch("sett.workflows.upload_keys.logger")
    @patch("sett.workflows.upload_keys.crypt_upload_keys")
    @patch("sett.workflows.upload_keys.search_pub_key")
    def test_upload_keys(
        self,
        mock_search_pub_key: Mock,
        mock_crypt_upload_keys: Mock,
        mock_logger: Mock,
        mock_crypt_request_verify: Mock,
        mock_upload_cert: Mock,
    ) -> None:
        # Set return values for functions called by the upload keys workflow.
        mock_search_pub_key.side_effect = TestUploadKeys.key_mock()
        mock_crypt_upload_keys.return_value = (
            Mock(
                key_fpr=str,
                status={self.email: VksEmailStatus.PUBLISHED},
                token="abcdef",
            ),
        )

        # Test the different argument combinations.
        for keyserver_url, key_fingerprints, should_raise_error in (
            # No Keyserver URL in config -> UserError.
            (None, self.key_fingerprints, True),
            # Runs expected to succeed:
            # Note: in the second case, no keys to upload are specified, but
            # that should not lead to an error.
            (self.keyserver, self.key_fingerprints, False),
            (self.keyserver, [], False),
        ):
            # Reset call counters on Mock objects.
            for m in (
                mock_search_pub_key,
                mock_crypt_upload_keys,
                mock_logger,
            ):
                m.reset_mock()

            # Make sure an error is raised for runs expected to fail.
            with self.assert_error_raised_if_needed(
                should_raise_error
            ), DisableLogging():
                # Run command to test.
                upload_keys(
                    fingerprints=key_fingerprints,
                    config=Mock(keyserver_url=keyserver_url, legacy_mode=True),
                )

                # Verify functions in the workflow were called as expected.
                if key_fingerprints:
                    assert mock_crypt_upload_keys.call_count == 2
                    assert mock_logger.info.call_count == 2
                    mock_crypt_request_verify.assert_not_called()
                else:
                    mock_crypt_upload_keys.assert_not_called()
                    mock_logger.info.assert_not_called()
                mock_upload_cert.assert_not_called()

    @patch("sett.workflows.upload_keys.search_pub_key")
    def test_upload_keys_no_uid(
        self,
        mock_search_pub_key: Mock,
    ) -> None:
        # Returns a key without any UID
        mock_search_pub_key.side_effect = lambda k, c, sigs: Mock(
            key_id=k, fingerprint=k, uids=()
        )
        with self.assert_error_raised_if_needed(True):
            upload_keys(
                fingerprints=self.key_fingerprints,
                config=Mock(keyserver_url=self.keyserver, legacy_mode=True),
            )

    @patch("sett.workflows.upload_keys.upload_cert")
    @patch("sett.workflows.upload_keys.crypt_request_key_verification")
    @patch("sett.workflows.upload_keys.crypt_upload_keys")
    @patch("sett.workflows.upload_keys.search_pub_key")
    def test_upload_keys_with_verification(
        self,
        mock_search_pub_key: Mock,
        mock_crypt_upload_keys: Mock,
        mock_crypt_request_verify: Mock,
        mock_upload_cert: Mock,
    ) -> None:
        mock_search_pub_key.side_effect = TestUploadKeys.key_mock()
        mock_crypt_upload_keys.return_value = (
            Mock(
                key_fpr=str,
                status={self.email: VksEmailStatus.UNPUBLISHED},
                token="abcdef",
            ),
        )
        upload_keys(
            fingerprints=self.key_fingerprints,
            config=Mock(keyserver_url=self.keyserver, legacy_mode=True),
        )
        self.assertEqual(mock_crypt_request_verify.call_count, 2)
        mock_upload_cert.assert_not_called()

    @patch("sett.workflows.upload_keys.upload_cert")
    @patch("sett.workflows.upload_keys.crypt_request_key_verification")
    @patch("sett.workflows.upload_keys.crypt_upload_keys")
    @patch("sett.workflows.upload_keys.search_pub_key")
    def test_upload_keys_with_revoked_key(
        self,
        mock_search_pub_key: Mock,
        mock_crypt_upload_keys: Mock,
        _: Mock,
        mock_upload_cert: Mock,
    ) -> None:
        mock_search_pub_key.side_effect = TestUploadKeys.key_mock()
        for status, should_raise_error in (
            (VksEmailStatus.UNPUBLISHED, False),
            (VksEmailStatus.PENDING, False),
            (VksEmailStatus.REVOKED, True),
        ):
            mock_crypt_upload_keys.return_value = (
                Mock(
                    key_fpr=str,
                    status={self.email: status},
                    token="abcdef",
                ),
            )
            with self.assert_error_raised_if_needed(should_raise_error):
                upload_keys(
                    fingerprints=self.key_fingerprints,
                    config=Mock(keyserver_url=self.keyserver, legacy_mode=True),
                )
            mock_upload_cert.assert_not_called()
