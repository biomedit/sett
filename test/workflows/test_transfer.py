import contextlib
from io import BytesIO
from unittest import mock
from typing import Generator, IO, Iterable, Any

from libbiomedit.lib.secret import Secret

from sett.protocols.sftp import Protocol as SftpProtocol
from sett.protocols.liquid_files import Protocol as LiquidfilesProtocol
from sett.workflows.encrypt import DATE_FMT_FILENAME
from sett.workflows.transfer import check_archive_name_follows_convention, transfer

from . import (
    DisableLogging,
    TestCaseWithRaiseError,
    MockPortalAPI,
    CHUCK_NORRIS_KEY,
    SGT_HARTMANN_KEY,
    mock_metadata_generator,
    encode_metadata_as_file,
)

MOCK_SFTP_PROTOCOL = SftpProtocol(
    host="sftp.roundhouse.gov",
    username="Chuck Norris",
    destination_dir="/upload",
    pkey="Chuck Norris needs no key... never has, never will.",
)

MOCK_LIQUIDFILES_PROTOCOL = LiquidfilesProtocol(
    host="server.roundhouse.gov",
    api_key=Secret("let's keep this between us"),
    recipients="Chuck Norris",
)


class TestTransfer(TestCaseWithRaiseError):
    def test_check_archive_name_follows_convention(self) -> None:
        # Test the different argument combinations.
        for file_name, project_code, suffix, should_raise_error in (
            # Runs expected to succeed:
            # Different valid argument combinations. No error should be raised.
            ("20210908T140302", None, None, False),
            ("demo_project_20210908T140302", "demo_project", None, False),
            ("20210908T140302_test", None, "test", False),
            ("demo_project_20210908T140302_test", "demo_project", "test", False),
            ("/something/random/demo_20210908T140302", "demo", None, False),
            ("20210908T140302", None, "test", False),
            ("demo_project_20210908T140302", "demo_project", "test", False),
            #
            # Runs expected to fail:
            # Wrong date-and-time format -> UserError.
            ("20210908T14032", None, None, True),
            ("202198T1432", None, None, True),
            ("2198T1432", None, None, True),
            ("20210908X140302", None, None, True),
            # Bad prefix in file name -> UserError.
            ("nemo_project_20210908T140302", "demo_project", None, True),
            # Bad suffix in file name -> UserError.
            ("demo_project_20210908T140302_test2", "demo_project", "test", True),
            ("demo_project_20210908T140302-a_secret", "demo_project", "test", True),
            ("demo_project_20210908T140302_a_secret", "demo_project", None, True),
            ("demo_project_20210908T140302-a_secret", "demo_project", None, True),
        ):
            # Run test for different file extensions.
            for extension in (".zip", ".tar", ".txt"):
                # When file extension is different from ".zip"/".tar", an error
                # should always be raised.
                if extension == ".txt":
                    should_raise_error = True

                # Verify that the check succeeds/fails as expected.
                with self.assert_error_raised_if_needed(should_raise_error) as e:
                    check_archive_name_follows_convention(
                        archive_path=file_name + extension,
                        project_code=project_code,
                        package_name_suffix=suffix,
                    )

                # When the check fails, verify the error message is correct.
                if should_raise_error:
                    expected_format = "_".join(
                        filter(None, (project_code, DATE_FMT_FILENAME, suffix))
                    )
                    self.assertIn(expected_format, str(e.exception))

    def test_transfer(self) -> None:
        """Test the main workflow."""

        @contextlib.contextmanager
        def mock_extract_multiple(
            *args: Any,
        ) -> Generator[Iterable[IO[bytes]], None, None]:
            _ = args
            yield (
                BytesIO(encode_metadata_as_file(metadata)),
                BytesIO(b"fake encrypted data"),
            )

        # Create mock configuration and data transfer protocol objects.
        config = mock.Mock(portal_api=MockPortalAPI())
        p = MOCK_SFTP_PROTOCOL
        p_lf = MOCK_LIQUIDFILES_PROTOCOL

        # Create mock metadata.
        meta_ok = mock_metadata_generator()
        meta_bad_dtrid = mock_metadata_generator(transfer_id=22)
        meta_no_dtrid = mock_metadata_generator(transfer_id=None)

        # Input file names for test.
        project_code = "ROUNDHOUSEKICK"
        suffix = "suffix"
        f_name = "20220208T152822"
        f_with_prefix = f"{project_code}_{f_name}"
        f_with_suffix = f"{f_name}_{suffix}"
        f_pre_and_suffix = f"{project_code}_{f_name}_{suffix}"
        e = ".zip"

        # Mock functions called by the transfer workflow.
        mock_check_package = mock.Mock(return_value=None)
        mock_retrieve_refresh_and_validate_keys = mock.Mock(
            return_value=(CHUCK_NORRIS_KEY, CHUCK_NORRIS_KEY, SGT_HARTMANN_KEY)
        )
        mock_sftp_upload = mock.Mock()
        mock_liquidfiles_upload = mock.Mock()

        # Run tests with different argument combinations.
        case_no = 0
        for (
            archive_files,
            extension,
            protocol,
            pkg_name_suffix,
            verify_dtr,
            verify_pkg_name,
            metadata,
            dry_run,
            should_raise_error,
        ) in (
            # Runs expected to fail:
            # Filename is missing project code -> not following convention -> UserError.
            ([f_name], e, p, None, True, True, meta_ok, False, True),
            # Filename has suffix when it should not -> UserError.
            ([f_pre_and_suffix], e, p, None, True, True, meta_ok, False, True),
            # No DTR ID checking -> no project code as prefix -> UserError
            # (because we are here specifying a fine name with project core prefix).
            ([f_with_prefix], e, p, suffix, False, True, meta_ok, False, True),
            # Missing DTR ID in the metadata of the file to transfer when
            # DTR ID checking is requested -> UserError.
            # In the second run, verify_dtr is set to 'None', meaning that the
            # checking of the DTR ID is conditional upon the presence of a DTR
            # ID in the metadata.
            ([f_with_prefix], e, p, None, True, True, meta_bad_dtrid, False, True),
            ([f_with_prefix], e, p, None, None, True, meta_bad_dtrid, False, True),
            # Verify DTR is requested but no DTR ID in the metadata -> UserError.
            ([f_with_prefix], e, p, None, True, True, meta_no_dtrid, False, True),
            #
            # Runs expected to succeed:
            # Filenames follow the expected convention.
            ([f_with_prefix], e, p, None, True, True, meta_ok, False, False),
            ([f_name], e, p, None, False, True, meta_ok, False, False),
            ([f_pre_and_suffix], e, p, suffix, True, True, meta_ok, False, False),
            # Filenames that do not follow conventions, but checking is disabled.
            ([f_with_prefix], e, p, None, True, False, meta_ok, False, False),
            ([f_with_prefix], e, p, None, None, False, meta_ok, False, False),
            ([f_pre_and_suffix], e, p, suffix, True, False, meta_ok, False, False),
            # Suffix in filename is optional, so no error should be raised if
            # it is missing.
            ([f_with_prefix], e, p, suffix, True, True, meta_ok, False, False),
            # No DTR ID checking means no prefix in filename.
            ([f_name, f_with_suffix], e, p, suffix, False, True, meta_ok, False, False),
            # Dry run.
            ([f_with_prefix] * 3, e, p, None, True, True, meta_ok, True, False),
            # Use LiquidFiles protocol.
            ([f_with_prefix], e, p_lf, None, True, True, meta_ok, False, False),
        ):
            print(f"CASE NUMBER: {case_no}")
            case_no += 1
            # Add extension to file names.
            archive_files = [f + extension for f in archive_files]

            # Reset call counters on Mock objects.
            for m in (
                mock_check_package,
                mock_retrieve_refresh_and_validate_keys,
                mock_sftp_upload,
                mock_liquidfiles_upload,
            ):
                m.reset_mock()

            # Replace functions called in the workflow by mock objects.
            with mock.patch(
                "sett.workflows.transfer.check_package", mock_check_package
            ), mock.patch(
                "sett.workflows.transfer.extract_multiple", mock_extract_multiple
            ), mock.patch(
                "sett.workflows.transfer.retrieve_refresh_and_validate_keys",
                mock_retrieve_refresh_and_validate_keys,
            ), mock.patch.object(
                SftpProtocol, "upload", mock_sftp_upload
            ), mock.patch.object(
                LiquidfilesProtocol, "upload", mock_liquidfiles_upload
            ), DisableLogging():
                # Make sure an error is raised for runs expected to fail.
                with self.assert_error_raised_if_needed(should_raise_error):
                    config.verify_dtr = verify_dtr
                    config.verify_package_name = verify_pkg_name
                    config.legacy_mode = True
                    # Run command to test.
                    transfer(
                        files=archive_files,
                        protocol=protocol,
                        config=config,
                        two_factor_callback=mock.Mock(),
                        dry_run=dry_run,
                        pkg_name_suffix=pkg_name_suffix,
                        progress=None,
                    )

                    # In dry-run mode, make sure that data is not uploaded.
                    if dry_run:
                        mock_sftp_upload.assert_not_called()
                        mock_liquidfiles_upload.assert_not_called()
                        continue

                    # Verify the correct protocol (sftp/liquidfiles) was called.
                    if protocol is p:
                        mock_sftp_upload.assert_called_once()
                        mock_liquidfiles_upload.assert_not_called()
                    elif protocol is p_lf:
                        mock_sftp_upload.assert_not_called()
                        mock_liquidfiles_upload.assert_called_once()
