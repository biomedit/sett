import os
import unittest
from unittest import mock

from sett.utils.config import Config
from sett.core.archive import DATA_FILE_ENCRYPTED, DATA_ARCHIVE
from sett.workflows.decrypt import decrypt_archive_gnupg, decrypt

from . import (
    DisableLogging,
    CHUCK_NORRIS_KEY,
    SGT_HARTMANN_KEY,
    MockProgressbar,
    CHUCK_NORRIS_PWD,
)


# Define argument values used throughout the different tests.
ARCHIVE_FILE = "20220131T152822.zip"
ARCHIVE_FILES = [ARCHIVE_FILE, "20220131T152823.zip"]
OUTPUT_DIR = os.path.join(os.sep, "fake", "output", "directory")
DATA_SIZE = 268435456
CONFIG = Config()


class TestDecrypt(unittest.TestCase):
    def test_decrypt_archive(self) -> None:
        # Define argument value
        checksum_file = os.path.join(
            OUTPUT_DIR, os.path.splitext(ARCHIVE_FILE)[0], "checksum.sha256"
        )

        # Mock functions called by decrypt_archive.
        mock_verify_metadata_signature = mock.Mock()
        mock_extract = mock.MagicMock()
        mock_retrieve_refresh_and_validate_keys = mock.Mock(
            return_value=[CHUCK_NORRIS_KEY, SGT_HARTMANN_KEY]
        )
        mock_check_password_matches_any_key = mock.Mock()
        mock_extract_with_progress = mock.MagicMock()
        mock_makedirs = mock.Mock()
        mock_core_decrypt = mock.Mock(return_value=(CHUCK_NORRIS_KEY.fingerprint,))
        mock_open = mock.MagicMock()
        mock_getsize = mock.Mock(return_value=DATA_SIZE)
        mock_gpg_store = mock.Mock()
        mock_gpg_store.list_sec_keys.return_value = [CHUCK_NORRIS_KEY, SGT_HARTMANN_KEY]
        mock_open_gpg_dir = mock.Mock(return_value=mock_gpg_store)

        # Run tests with different argument combinations.
        for decrypt_only, progress in (
            (True, None),
            (False, None),
            (False, MockProgressbar()),
        ):
            # Reset call counters on Mock objects.
            for m in (
                mock_verify_metadata_signature,
                mock_extract,
                mock_retrieve_refresh_and_validate_keys,
                mock_check_password_matches_any_key,
                mock_extract_with_progress,
                mock_makedirs,
                mock_core_decrypt,
                mock_open,
                mock_getsize,
            ):
                m.reset_mock()

            # Replace functions called in the workflow by mock objects.
            with mock.patch(
                "sett.workflows.decrypt.verify_metadata_signature",
                mock_verify_metadata_signature,
            ), mock.patch("sett.workflows.decrypt.extract", mock_extract), mock.patch(
                "sett.workflows.decrypt.retrieve_refresh_and_validate_keys",
                mock_retrieve_refresh_and_validate_keys,
            ), mock.patch(
                "sett.workflows.decrypt.check_password_matches_any_key",
                mock_check_password_matches_any_key,
            ), mock.patch(
                "sett.workflows.decrypt.extract_with_progress",
                mock_extract_with_progress,
            ), mock.patch(
                "os.makedirs", mock_makedirs
            ), mock.patch(
                "sett.workflows.decrypt.core_decrypt", mock_core_decrypt
            ), mock.patch(
                "sett.workflows.decrypt.open", mock_open
            ), mock.patch(
                "os.path.getsize", mock_getsize
            ), mock.patch(
                "sett.utils.config.open_gpg_dir", mock_open_gpg_dir
            ), DisableLogging():
                # Run command to test.
                decrypt_archive_gnupg(
                    archive_file=ARCHIVE_FILE,
                    passphrase=CHUCK_NORRIS_PWD,
                    output_dir=OUTPUT_DIR,
                    config=CONFIG,
                    decrypt_only=decrypt_only,
                    progress=progress,
                )

            # Verify that the different functions being called during the
            # decrypt workflow are called with the expected arguments.
            mock_verify_metadata_signature.assert_called_once_with(
                tar_file=ARCHIVE_FILE,
                gpg_store=mock_gpg_store,
                keyserver_url=CONFIG.keyserver_url,
                allow_key_download=CONFIG.allow_gpg_key_autodownload,
            )
            mock_extract.assert_called_once_with(ARCHIVE_FILE, DATA_FILE_ENCRYPTED)
            mock_check_password_matches_any_key.assert_called_once_with(
                password=CHUCK_NORRIS_PWD,
                keys=[CHUCK_NORRIS_KEY, SGT_HARTMANN_KEY],
                gpg_store=mock_gpg_store,
            )
            mock_extract_with_progress.assert_called_once_with(
                ARCHIVE_FILE,
                mock.ANY if progress else None,
                DATA_FILE_ENCRYPTED,
            )
            mock_makedirs.assert_called_once_with(
                os.path.dirname(checksum_file), exist_ok=True
            )
            mock_core_decrypt.assert_called_once_with(
                source=mock.ANY,
                output=os.path.join(
                    OUTPUT_DIR,
                    os.path.splitext(ARCHIVE_FILE)[0],
                    DATA_ARCHIVE,
                )
                if decrypt_only
                else mock.ANY,
                gpg_store=mock_gpg_store,
                passphrase=CHUCK_NORRIS_PWD,
            )
            mock_getsize.assert_called_once_with(ARCHIVE_FILE)

            # Check that retrieve_refresh_and_validate_keys was called once.
            mock_retrieve_refresh_and_validate_keys.assert_called_once()
            mock_retrieve_refresh_and_validate_keys.assert_called_with(
                key_identifiers=(CHUCK_NORRIS_KEY.fingerprint,), config=CONFIG
            )

            # The call to "open" to read the content of the data.tar.gz
            # file is only made if both decryption and decompression are
            # carried-out.
            if decrypt_only:
                mock_open.assert_not_called()
            else:
                mock_open.assert_called_once_with(checksum_file, "rb")

    def test_decrypt(self) -> None:
        # Mock functions called by the decrypt workflow.
        mock_check_package = mock.Mock()
        mock_check_space = mock.Mock()
        mock_get_total_size = mock.Mock(return_value=DATA_SIZE)
        mock_decrypt_archive = mock.Mock()

        # Run tests with different argument combinations.
        for dry_run, progress in (
            (True, None),
            (False, None),
            (False, MockProgressbar()),
        ):
            # Reset call counters on Mock objects.
            for m in (
                mock_check_package,
                mock_check_space,
                mock_get_total_size,
                mock_decrypt_archive,
            ):
                m.reset_mock()

            # Replace functions called in the workflow by mock objects.
            with mock.patch(
                "sett.workflows.decrypt.check_package", mock_check_package
            ), mock.patch(
                "sett.workflows.decrypt.check_space", mock_check_space
            ), mock.patch(
                "sett.workflows.decrypt.get_total_size", mock_get_total_size
            ), mock.patch(
                "sett.workflows.decrypt.decrypt_archive", mock_decrypt_archive
            ), DisableLogging():
                # Run command to test.
                decrypt(
                    files=ARCHIVE_FILES,
                    passphrase=CHUCK_NORRIS_PWD,
                    output_dir=OUTPUT_DIR,
                    config=CONFIG,
                    decrypt_only=False,
                    dry_run=dry_run,
                    progress=progress,
                )

            # Verify that the different functions being called during the
            # decrypt workflow are called with the expected arguments.
            self.assertEqual(mock_check_package.call_count, len(ARCHIVE_FILES))
            for f in ARCHIVE_FILES:
                mock_check_package.assert_any_call(f)
            mock_check_space.assert_called_once_with(DATA_SIZE, OUTPUT_DIR, force=False)

            # Function "decrypt_archive" is only called when not in
            # "dry run" mode. The function is called once for each input
            # archive to decrypt.
            if dry_run:
                mock_decrypt_archive.assert_not_called()
            else:
                self.assertEqual(mock_decrypt_archive.call_count, len(ARCHIVE_FILES))
                for f in ARCHIVE_FILES:
                    mock_decrypt_archive.assert_any_call(
                        f,
                        passphrase=CHUCK_NORRIS_PWD,
                        output_dir=OUTPUT_DIR,
                        config=CONFIG,
                        decrypt_only=False,
                        progress=progress,
                    )
