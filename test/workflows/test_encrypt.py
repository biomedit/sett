import os
import io
import re
import contextlib
import tempfile
from pathlib import Path
from unittest import mock
from datetime import datetime
from typing import Any, IO, Iterator, Sequence, Generator, Optional

from sett.core import gpg
from sett.core.error import UserError
from sett.core.metadata import Purpose
from sett.workflows.encrypt import (
    generate_output_archive_name,
    check_paths_on_posix,
    verify_dtr_info_and_get_project_code,
    check_arg_value,
    byte_encode_metadata,
    check_integer_in_range,
    check_path,
    encrypt,
    DATE_FMT_FILENAME,
)
from sett.utils.config import Config
from . import (
    DisableLogging,
    TestCaseWithRaiseError,
    CHUCK_NORRIS_KEY,
    SGT_HARTMANN_KEY,
    FAKE_CHUCK_NORRIS_KEY,
    CHUCK_NORRIS_PWD,
    TEST_PROJECT_CODE,
    mock_metadata_generator,
    encode_metadata_as_file,
    MockProgressbar,
    MockPortalAPI,
)


class MockGPGStore(mock.Mock):
    """A fake GPG store for testing purposes."""

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.detached_signature = "mock detached signature"

    @contextlib.contextmanager
    def detach_sig(self, *args: Any, **kwargs: Any) -> Generator[IO[bytes], None, None]:
        _ = args
        _ = kwargs
        yield io.BytesIO(self.detached_signature.encode())


def mock_pathlib_exists(path: Path) -> bool:
    """Mocks the pathlib.Path.exists() method, except for the current working
    directory.
    """
    return path == Path.cwd() or not str(path).startswith(os.path.join(os.sep, "fake"))


def mock_pathlib_is_dir(self: Path) -> bool:
    """Mocks the pathlib.Path.is_dir() method, except for the current working
    directory.
    """
    return self == Path.cwd() or str(self).endswith("directory")


def generate_output_name_regexp(prefix: Optional[str], suffix: Optional[str]) -> str:
    """Generate a regexp to test the name of an archive file."""
    regexp = "[0-9]{8}T[0-9]{6}"
    if prefix:
        regexp = f"{prefix}_" + regexp
    if suffix:
        regexp += f"_{suffix}"
    regexp += ".zip"
    return regexp


class TestEncrypt(TestCaseWithRaiseError):
    # Define fake existing and non-existing directories and files.
    #  -> non-existing paths start with "/fake"
    #  -> directories end in "directory".
    #  -> non-writable directories end in "non-writable".
    existing_dir = os.path.join(os.sep, "almost", "a", "real", "directory")
    existing_file = os.path.join(existing_dir, "test_file")
    non_existing_dir = os.path.join(os.sep, "fake", "directory")
    non_existing_file = os.path.join(non_existing_dir, "test_file")
    non_writable_dir = os.path.join(existing_dir, "non-writable")

    def test_generate_output_archive_name(self) -> None:
        # Argument values used in this test.
        cwd = os.getcwd()
        name = "roundhouse_kicks_for_everyone"
        time = datetime.strptime("20220131T152822", DATE_FMT_FILENAME)
        full_name = "prefix_20220131T152822_suffix.zip"
        n_override = name + ".zip"

        # Test the different argument combinations.
        for (
            prefix,
            suffix,
            timestamp,
            override_name,
            override_dir,
            should_raise_error,
            expected_name,
        ) in (
            # Test name combinations without override.
            (None, None, time, None, None, False, "20220131T152822.zip"),
            ("prefix", None, time, None, None, False, "prefix_20220131T152822.zip"),
            (None, "suffix", time, None, None, False, "20220131T152822_suffix.zip"),
            ("prefix", "suffix", time, None, None, False, full_name),
            # Test overriding name.
            ("prefix", "suffix", time, name, None, False, n_override),
            ("prefix", "suffix", time, name + ".zip", None, False, n_override),
            # Test overriding directory.
            ("prefix", "suffix", time, None, self.existing_dir, False, full_name),
            ("prefix", "suffix", time, None, cwd, False, full_name),
            # Test overriding name and directory.
            ("prefix", "suffix", time, name, self.existing_dir, False, n_override),
            ("prefix", "suffix", time, name, cwd, False, n_override),
            # Test missing time stamp.
            (None, None, None, None, None, False, "20220131T152822.zip"),
            ("prefix", "suffix", None, None, None, False, "20220131T152822.zip"),
            # Test overriding with non-existing directory -> UserError.
            ("prefix", "suffix", time, name, self.non_existing_dir, True, n_override),
            # Test overriding with non-writable directory -> UserError.
            ("prefix", "suffix", time, name, self.non_writable_dir, True, n_override),
        ):
            with mock.patch(
                "os.access", lambda x, _: not x.endswith("non-writable")
            ), mock.patch.object(
                Path, "exists", mock_pathlib_exists
            ), mock.patch.object(
                Path, "is_dir", mock_pathlib_is_dir
            ):
                # Construct the override path from the directory and file names.
                if override_dir and override_name:
                    dir_or_name_override = os.path.join(override_dir, override_name)
                elif override_dir:
                    dir_or_name_override = override_dir
                elif override_name:
                    dir_or_name_override = override_name
                else:
                    dir_or_name_override = None

                # Test that a non-existing directory raises an error.
                with self.assert_error_raised_if_needed(should_raise_error):
                    archive_name = generate_output_archive_name(
                        prefix=prefix,
                        timestamp=timestamp,
                        suffix=suffix,
                        dir_or_name_override=dir_or_name_override,
                    )

                    # Special case: no timestamp was provided, the function
                    # uses the current time instead.
                    if not timestamp:
                        regexp = generate_output_name_regexp(prefix, suffix)
                        self.assertTrue(
                            re.fullmatch(regexp, os.path.basename(archive_name)),
                            msg=f"Output archive '{archive_name}' should be "
                            f"named '{regexp}'",
                        )
                        continue

                    # Verify the generated output name matches the expected name.
                    self.assertIsInstance(archive_name, str)
                    expected_path = (
                        Path(override_dir or cwd).joinpath(expected_name).as_posix()
                    )
                    self.assertEqual(
                        archive_name,
                        expected_path,
                        msg=f"generated name '{archive_name}' does not match "
                        f"with expected name '{expected_path}'.",
                    )

    def test_check_paths_on_posix(self) -> None:
        # Define "good" (posix compliant) and "bad" (non-posix) paths.
        good_path_1 = "foo/bar.txt"
        good_path_2 = "/foo/baz/test.csv"
        bad_path = "foo\\baz\\test.csv"

        # Test function with good path. No error should be raised.
        check_paths_on_posix((good_path_1, good_path_2))

        # Verify that a bad path raises an error.
        with mock.patch("sett.core.checksum.os.path.sep", "/"), self.assertRaises(
            UserError
        ):
            check_paths_on_posix((good_path_1, bad_path))

    def test_verify_dtr_info_and_get_project_code(self) -> None:
        # Define "good" (authorized) and "bad" (non-authorized, not project DM)
        # data senders and recipients.
        sender_good = CHUCK_NORRIS_KEY
        sender_bad = FAKE_CHUCK_NORRIS_KEY
        recipients_good = [CHUCK_NORRIS_KEY, SGT_HARTMANN_KEY]
        recipients_bad = [FAKE_CHUCK_NORRIS_KEY, SGT_HARTMANN_KEY]

        # Define config objects with or without portal API.
        c = mock.Mock(portal_api=MockPortalAPI())
        c_no_portal = mock.Mock(portal_api=None)

        # Test the different argument combinations.
        for (
            dtr_id,
            purpose,
            config,
            sender_pub_key,
            recipients_pub_key,
            should_raise_error,
        ) in (
            # Runs expected to fail:
            # Test missing DTR ID -> UserError.
            (None, Purpose.TEST, c, sender_good, recipients_good, True),
            # Test missing purpose -> UserError.
            (23, None, c, sender_good, recipients_good, True),
            # Test missing portal API -> UserError.
            (23, Purpose.TEST, c_no_portal, sender_good, recipients_good, True),
            # Test bad DTR ID, bad sender, bad recipients, wrong purpose -> UserError.
            (22, Purpose.TEST, c, sender_good, recipients_good, True),
            (23, Purpose.TEST, c, sender_bad, recipients_good, True),
            (23, Purpose.TEST, c, sender_good, recipients_bad, True),
            (23, Purpose.PRODUCTION, c, sender_good, recipients_good, True),
            #
            # Runs expected to succeed:
            (23, Purpose.TEST, c, sender_good, recipients_good, False),
        ):
            # Make sure an error is raised for runs expected to fail.
            with self.assert_error_raised_if_needed(
                should_raise_error
            ), DisableLogging():
                # Run the command to test.
                project_code = verify_dtr_info_and_get_project_code(
                    dtr_id=dtr_id,
                    purpose=purpose,
                    config=config,
                    sender_fingerprint=sender_pub_key.fingerprint,
                    recipients_fingerprint=[
                        key.fingerprint for key in recipients_pub_key
                    ],
                )

                # Verify that the correct project code was returned.
                self.assertEqual(project_code, c.portal_api.project_code)

    def test_check_arg_value(self) -> None:
        # Define a simple test type checker function that raises an error if
        # the passed value is not a string.
        def arg_type_checker(x: Any) -> None:
            if not isinstance(x, str):
                raise ValueError()

        # Verify that an error is raised if an object of the wrong type is
        # passed to the arg_type_checker.
        for arg_value, should_raise_error in (("foo", False), (32, True)):
            with self.assert_error_raised_if_needed(should_raise_error):
                check_arg_value(
                    arg_value=arg_value,
                    arg_name="test_argument",
                    arg_type_checker=arg_type_checker,
                )

    def test_byte_encode_metadata(self) -> None:
        # Create fake metadata and GPG store objects.
        mock_metadata = mock_metadata_generator()
        mock_metadata_encoded = encode_metadata_as_file(mock_metadata)
        mock_gpg_store = MockGPGStore()

        # Run tests with both with and without creating a detached signature.
        for sender_pub_key in (CHUCK_NORRIS_KEY, None):
            # Execute the function to test.
            encoded_metadata, encoded_signature = byte_encode_metadata(
                metadata=mock_metadata,
                gpg_store=mock_gpg_store,
                passphrase=CHUCK_NORRIS_PWD,
                sender_pub_key=sender_pub_key,
            )

            # Check output matches the expected return values.
            expected_encoded_signature = (
                mock_gpg_store.detached_signature.encode() if sender_pub_key else b""
            )
            self.assertEqual(encoded_metadata, mock_metadata_encoded)
            self.assertEqual(encoded_signature, expected_encoded_signature)

    def test_check_integer_in_range(self) -> None:
        # Run the function and verify an error is raised when the checked
        # value is not in the specified range.
        check_value = check_integer_in_range(min_value=0, max_value=10)
        for x in (-1, 11, None, "foo"):
            with self.assertRaises(ValueError):
                check_value(x)
        for x in (0, 5, 10):
            check_value(x)

    def test_check_path(self) -> None:
        # To run these tests, the "exists" and "is_dir" methods of Path
        # must be mocked, so that we don't need to use real paths on disk.
        with mock.patch.object(Path, "exists", mock_pathlib_exists), mock.patch.object(
            Path, "is_dir", mock_pathlib_is_dir
        ):
            # Test case where file or directory exists and no error is raised.
            check_path(directory=False, writable=False)(self.existing_file)
            check_path(directory=True, writable=False)(self.existing_dir)

            # Test case where a file does not exist.
            with self.assertRaises(ValueError):
                check_path(directory=False, writable=False)(self.non_existing_file)

            # Test case where a file exists, but it should be a directory.
            with self.assertRaises(ValueError):
                check_path(directory=True, writable=False)(self.existing_file)

            # Test case where directory exists, but is not writable.
            with self.assertRaises(ValueError):
                check_path(directory=True, writable=True)(self.existing_dir)

    # pylint: disable=too-many-statements
    def test_encrypt(self) -> None:
        """Test main workflow."""

        # Define a version of 'search_files_recursively' that can handle fake
        # paths.
        def mock_search_files_recursively(input_paths: Sequence[str]) -> Iterator[str]:
            for path in input_paths:
                if path.endswith("directory"):
                    yield os.path.join(path, "fake_file")
                if path.endswith("empty"):
                    continue
                yield path

        # Define fake directories and files.
        fake_dir = os.path.join(os.sep, "fake", "directory")
        fake_file = os.path.join(os.sep, "fake", "test_file")
        fake_empty_directory = os.path.join(os.sep, "fake", "directory", "empty")
        data_size = 268435456

        # Mock functions called by the encrypt workflow.
        mock_check_file_read_permission = mock.Mock()
        mock_retrieve_refresh_and_validate_keys = mock.Mock(
            return_value=(CHUCK_NORRIS_KEY, CHUCK_NORRIS_KEY, SGT_HARTMANN_KEY)
        )
        mock_search_priv_key = mock.Mock(return_value=CHUCK_NORRIS_KEY)
        mock_verify_dtr_info_and_get_project_code = mock.Mock(
            return_value=TEST_PROJECT_CODE
        )
        mock_get_total_size = mock.Mock(return_value=data_size)
        mock_check_space = mock.Mock()
        mock_check_password = mock.Mock()
        mock_generate_checksums_file_content = mock.Mock(return_value="test")
        mock_os_path_getsize = mock.Mock(return_value=data_size)
        mock_encrypt_and_sign = mock.Mock()
        mock_stringIO = mock.Mock()
        mock_stringIO.return_value.read.return_value = (
            "d4c266918facb1ab5b45c6b16709b317cdfa73e8333c9de5778f36f3f6418da1"
        )
        mock_byte_encode_metadata = mock.Mock(return_value=(b"foo", b"bar"))

        # Arguments with fixed values.
        data_recipients = [CHUCK_NORRIS_KEY.fingerprint, SGT_HARTMANN_KEY.fingerprint]

        # Arguments with variable values to test.
        f = [fake_dir, fake_file]  # Input files.
        c = Config()
        c_with_suffix = Config(package_name_suffix="suffix_from_config")
        d = 23  # DTR ID.
        s = CHUCK_NORRIS_KEY.fingerprint  # Data sender.
        suffix = "custom_suffix"
        p = MockProgressbar()

        # Run tests with different argument combinations.
        for (
            files_to_encrypt,
            config,
            dtr_id,
            sender,
            output_name,
            output_suffix,
            verify_dtr,
            compression_level,
            dry_run,
            progress,
            should_raise_error,
        ) in (
            # Tests that should raise a UserError
            # * Missing files to encrypt.
            ([], c, d, s, None, None, True, 5, False, None, True),
            # * Encrypting an empty directory.
            ([fake_empty_directory], c, d, s, None, None, True, 5, False, None, True),
            # * Bad compression level.
            (f, c, d, s, None, None, True, 99, False, None, True),
            #
            # Tests that should pass.
            # * Encrypt workflow completing without error.
            (f, c, d, s, None, None, True, 5, False, None, False),
            # * Using a custom suffix.
            (f, c, d, s, None, suffix, True, 5, False, None, False),
            # * Using a custom suffix from config file.
            (f, c_with_suffix, d, s, None, None, True, 5, False, None, False),
            # * Using a custom name for the output file.
            (f, c, d, s, "custom_name", None, True, 5, False, None, False),
            # * Dry-run mode.
            (f, c, d, s, None, suffix, True, 5, True, None, False),
            # * No DTR verification.
            (f, c, d, s, None, None, False, 5, False, None, False),
            # * With a progress bar.
            (f, c, d, s, None, None, False, 5, False, p, False),
        ):
            # Reset call counters on Mock objects.
            for m in (
                mock_check_file_read_permission,
                mock_retrieve_refresh_and_validate_keys,
                mock_search_priv_key,
                mock_verify_dtr_info_and_get_project_code,
                mock_get_total_size,
                mock_check_space,
                mock_check_password,
                mock_generate_checksums_file_content,
                mock_os_path_getsize,
                mock_encrypt_and_sign,
                mock_stringIO,
                mock_byte_encode_metadata,
            ):
                m.reset_mock()

            # Replace functions called in the workflow by mock objects.
            with mock.patch(
                "sett.workflows.encrypt.search_files_recursively",
                mock_search_files_recursively,
            ), mock.patch(
                "sett.workflows.encrypt.check_file_read_permission",
                mock_check_file_read_permission,
            ), mock.patch(
                "sett.workflows.encrypt.retrieve_refresh_and_validate_keys",
                mock_retrieve_refresh_and_validate_keys,
            ), mock.patch(
                "sett.workflows.encrypt.search_priv_key",
                mock_search_priv_key,
            ), mock.patch(
                "sett.workflows.encrypt.verify_dtr_info_and_get_project_code",
                mock_verify_dtr_info_and_get_project_code,
            ), mock.patch(
                "sett.workflows.encrypt.get_total_size",
                mock_get_total_size,
            ), mock.patch(
                "sett.workflows.encrypt.check_space",
                mock_check_space,
            ), mock.patch(
                "sett.workflows.encrypt.check_password",
                mock_check_password,
            ), mock.patch(
                "sett.workflows.encrypt.generate_checksums_file_content",
                mock_generate_checksums_file_content,
            ), mock.patch(
                "os.path.getsize", mock_os_path_getsize
            ), mock.patch(
                "sett.workflows.encrypt.encrypt_and_sign", mock_encrypt_and_sign
            ), mock.patch(
                "io.StringIO", mock_stringIO
            ), mock.patch(
                "sett.workflows.encrypt.byte_encode_metadata", mock_byte_encode_metadata
            ), mock.patch.object(
                gpg.GPGStore, "default_key", lambda x: None
            ), DisableLogging():
                # Make sure an error is raised for runs expected to fail.
                with self.assert_error_raised_if_needed(
                    should_raise_error
                ), tempfile.TemporaryDirectory() as tmpdirname:
                    # Run command to test.
                    config.verify_dtr = verify_dtr
                    config.legacy_mode = True
                    encrypted_file_name = encrypt(
                        files=files_to_encrypt,
                        config=config,
                        sender=sender,
                        recipients=data_recipients,
                        dtr_id=dtr_id,
                        passphrase=CHUCK_NORRIS_PWD,
                        output=Path(tmpdirname)
                        .joinpath(output_name if output_name else "")
                        .as_posix(),
                        output_suffix=output_suffix,
                        dry_run=dry_run,
                        force=False,
                        compression_level=compression_level,
                        purpose=None,
                        progress=progress,
                    )

                    # In dry run mode, no output file name is produced.
                    if dry_run:
                        self.assertIsNone(encrypted_file_name)
                        mock_generate_checksums_file_content.assert_not_called()
                        mock_encrypt_and_sign.assert_not_called()
                        continue

                    # Check that output file name matches the expected pattern.
                    assert encrypted_file_name is not None  # needed for mypy.
                    pkg_name = os.path.basename(encrypted_file_name)
                    if output_name:
                        regexp = output_name + ".zip"
                    else:
                        regexp = generate_output_name_regexp(
                            prefix=TEST_PROJECT_CODE if verify_dtr else None,
                            suffix=output_suffix or config.package_name_suffix,
                        )
                    self.assertTrue(
                        re.fullmatch(regexp, pkg_name),
                        msg=f"Output filename '{pkg_name}' should be named '{regexp}'",
                    )

                    # Verify that the encrypt_and_sign function was called.
                    mock_encrypt_and_sign.assert_called_once()
                    mock_generate_checksums_file_content.assert_called_once()
                    mock_byte_encode_metadata.assert_called_once()

                    # Delete output file.
                    self.assertTrue(os.path.exists(encrypted_file_name))
