.. _PORTAL_URL: https://portal.dcc.sib.swiss
.. _KEYSERVER_URL: https://keys.openpgp.org
.. _OPENPGP_URL: https://www.openpgp.org
.. _GNUPG_URL: https://gnupg.org
.. _SEQUOIA_URL: https://sequoia-pgp.org

.. _section-pgp-key-management:

PGP key management with sett
============================

To encrypt and decrypt data, *sett* uses **public key cryptography**. If you
are not familiar with public key cryptography concepts such as **public and
private** keys, **revocation certificates**, or **keyservers** you are advised
to read `this introductory section <intro_to_pgp_>`_.


.. _section-generate-key:

Generate a new public/private PGP key pair
------------------------------------------

A prerequisite to encrypt, decrypt and transfer files with *sett* is to have a
public/private PGP key pair.

.. important::

    Always keep your **private key**, its **password**, and its
    **revocation certificate** in a secure location. Never share any of this
    information with anyone.

Generate a new key pair with sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To generate a new public/private key pair using the *sett* graphical user
interface:

#. Go to the **Keys** tab and click **Generate new private/public key**.
   A dialog box will appear.

   .. image:: ../img/sett_generate_key_01.*

#. Fill-in the required fields:

    * **Full name**: key owner first and last name.
    * **Institution/project**: optionally, enter the institution you are
      affiliated with.
    * **Institutional email**: enter the email to be associated with the key
      pair.
    * **Password**: the password must be at least 10 characters long, and it is
      highly recommended that it contain a mix of letters, numbers and special
      characters.

     .. warning::

         It is **not possible to retrieve a password from a generated key**,
         so make sure to remember it and store it in a safe place (e.g. a
         password manager).

#. Click **Generate key** and wait for a few seconds, a new key pair will be
   created.

#. A new pop-up window will appear to confirm that the key was generated
   successfully. Click **Show details** to display the revocation certificate
   associated with the new key, as shown in the figure below.

   Copy the revocation certificate to a safe location, ideally a password
   manager, and click **OK** to close the dialog box.

   .. image:: ../img/sett_generate_key_02.*

#. The new key should now be listed in *sett* under both **Private keys**
   and **Public keys**.

   .. image:: ../img/sett_generate_key_03.*

#. Now that your new PGP key is created, make sure to
   :ref:`Upload it to the keyserver<section-upload-key>`, and to
   :ref:`register it in the BioMedIT portal<section-register-key>`.

Generate a new key pair in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The *sett* command line interface does not implement new key generation.
Please use the GnuPG command line instead:

.. code-block:: shell

    gpg --full-generate-key

During the key generation process, a number of questions must be answered.
Here are the suggested settings:

.. code-block:: shell

    type of key      : (1) RSA and RSA (default)
    key bit length   : 4096
    key validity time: 0 (Key does not expire)
    Real name        : your full name (e.g. Alice Smith)
    Email address:   : your institutional email address, will be associated
                       permanently to your key.
    Comment          : optionally, a "comment" (e.g. the institution you work
                       for) can be added to the key metadata.

.. important::

    At the end of the key generation process, ``gpg`` will ask for a pass
    phrase (password) that will be associated with the private key. Please use
    a reasonably strong password and make sure to save it in a password
    manager.

    **If the password is forgotten, there is no way to retrieve or re-create
    it**. Your PGP key pair will thus become unusable and will need to be
    revoked.

After having created a new key, it is strongly recommended to immediately
generate a revocation certificate. Note that the password of the private key
must be entered in order to complete the commands below.

.. code-block:: shell

    # General syntax:
    gpg --output <revocation certificate file> --gen-revoke <key fingerprint>

    # Example:
    gpg --output revocation_certificate.asc --gen-revoke AD1008B5F0F876630B9AF245AA2BF155EF144550

List your local PGP keys in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Private and public keys available in the local keyrings can be listed with the
following commands:

.. code-block:: shell

    gpg --list-keys            # list public key(s).
    gpg --list-secret-keys     # list private key(s).


Export/import private keys in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In some situations (new computer setup, remote *sett* environment, ...) you
might need to copy or move your private key to a different machine. This can
be done by **exporting the private key** to a file, transferring the file to
the new machine, and **importing** it.

Here is an example of how to export a private key to a file with GnuPG:

.. code-block:: shell

    # The key identifier can be its fingerprint, key ID or the user's email address.
    # NOTE: the order of arguments matters, the '--output' option must be passed first!
    gpg --output private_key.pgp --export-secret-key username@email

You will be prompted for the key password. Note that keys can also be exported
in ASCII format (instead of binary) by adding the ``--armor`` option.

After the key has been transferred to the new machine (in either binary or
ASCII format), importing it is as easy as:

.. code-block:: shell

    gpg --import private.pgp

Verify that the key has been correctly imported with ``gpg --list-secret-keys``.

**Warning:** the above method exports only a single key, not the entire content
of your GnuPG keyring. If your intention is to create a backup of all your PGP
keys, follow the instructions given
`here <https://risanb.com/code/backup-restore-gpg-key/>`_ or have a look at
this
`procedure <https://gist.github.com/garetmckinley/29cab22251d1913eee9556ff5b286052>`_
instead.

Ensure that you store any backed-up secret keys in a secure location and in an
encrypted form (typically in a password manager).

.. _section-upload-key:

Upload your public PGP key to the keyserver
-------------------------------------------

*sett* allows users to upload their public PGP key to the keyserver specified
in its :ref:`configuration file<Configuration options>`.

.. admonition:: BioMedIT

  By default, a new *sett* installation is configured to upload keys to the
  `OpenPGP keyserver <KEYSERVER_URL_>`_. If you have used *sett* before, make
  sure your keyserver setting is no longer pointing to
  "keyserver.dcc.sib.swiss", as this has now been decommissioned.

  `keys.openpgp.org <KEYSERVER_URL_>`_ is a public keyserver for the
  distribution and discovery of public PGP keys. This service is GDPR-compliant,
  based on the open source software
  `Hagrid <https://gitlab.com/hagrid-keyserver/hagrid>`_,
  and already used by over 300'000 users worldwide. For more information about
  the service, please see `this link <https://keys.openpgp.org/about>`_.


Upload your public PGP key with sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Uploading your public PGP key to the keyserver specified in *sett* is
straightforward:

#. In the **Settings** tab, make sure that the **Keyserver URL** points to
   https://keys.openpgp.org.
#. In the **Keys** tab, select your public PGP key from the **Public keys**
   list.
#. Click on the **Upload selected keys to keyserver** icon (arrow pointing up
   into cloud).
#. A dialog box will appear to ask for confirmation.
#. To **verify your key** with the keyserver (i.e. associate your email address
   with your key on the keyserver), make sure that ``[x] Associate the key with
   your identity`` **is checked**. In practical terms, this means that people
   will now be able to search for your key on the keyserver using your email
   address - otherwise your key will only be searchable using its full
   fingerprint.
#. Click **OK**.
#. If you have selected the checkbox, **you will shortly receive an email**
   from keyserver@keys.openpgp.org that contains a confirmation link to prove
   that you have access to your email.
#. Open the email and **click on the link** to associate your key with your
   email address. Other users now can find your public key with your email
   address.


Upload your public PGP key in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Uploading keys via the command line can be done either using *sett* or *GnuPG*.
In both cases, you first need to **retrieve the fingerprint associated with
your PGP key**. The fingerprint is a 40 characters-long hexadecimal string.
This can be done with the following command:

.. code-block:: shell

    # Using a recent GnuPG version [>= 2.2.x]:
    gpg --list-secret-keys

    # Using older GnuPG versions: make sure to remove the spaces present in
    # the displayed fingerprint (a fingerprint is a 40 characters-long string
    # with no spaces).
    gpg --list-secret-keys --with-fingerprint

*Note*: if multiple keys are listed by the ``gpg --list-secret-keys`` command,
make sure you are taking the fingerprint from the key you actively use.

After having retrieved your key's fingerprint, you can now proceed with the
key upload, using either *sett* or *GnuPG*, as shown below.

**sett command:**

.. code-block:: shell

    # General upload syntax:
    sett verify-keylengths-and-upload-keys <key fingerprint>

    # Example:
    sett verify-keylengths-and-upload-keys AD1008B5F0F876630B9AF245AA2BF155EF144550

**GnuPG command:**

.. code-block:: shell

    # General syntax:
    gpg --keyserver https://keys.openpgp.org --send-key <key fingerprint>

    # Example:
    gpg --keyserver https://keys.openpgp.org --send-key AD1008B5F0F876630B9AF245AA2BF155EF144550


Upload your public PGP key using a web browser
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Export your **public PGP key** to a file using the command line given below
   in your shell/terminal/command prompt (replace ``email@example.com`` with
   the email or fingerprint of your key). This should work on all platforms
   (Linux, Mac, Windows).

   **Important:** make sure that a single key gets exported - e.g. if your
   email is associated with multiple keys, you should use the key's fingerprint
   instead of the email in the export command below.

   .. code-block:: shell

    # NOTE: the order of arguments matters, the '--output' option must be passed first!
    gpg --output public_key.pgp --export --armor email@example.com

    # Alternative if you have your email associated to multiple keys:
    # 1. Retrieve your key's fingerprint:
    gpg --list-secret-keys                      # For recent GnuPG versions [>= 2.2.x].
    gpg --list-secret-keys --with-fingerprint   # For older GnuPG versions.
    # 2. Export your key using its fingerprint as identifier.
    gpg --output public_key.pgp --export --armor AD1008B5F0F876630B9AF245AA2BF155EF144550

#. In your web browser, go to the keyserver's
   `upload page <https://keys.openpgp.org/upload>`_. Click on **Browse...**
   and select the file you just exported (e.g. ``public_key.pgp``).

   .. image:: ../img/keyserver_key_upload_1.*

#. Click on **Upload** (blue button to the right of the text field): your key
   will be uploaded to the keyserver, and you should see the following
   confirmation message displayed:

   .. image:: ../img/keyserver_key_upload_2.*

#. The next step is to **verify your key** with the keyserver by clicking on
   **Send Verification Email** . The keyserver will check that you have access
   to the email associated with the PGP key you just uploaded.

   After a key is verified, its fingerprint and email are published on the
   keyserver, and the key is then searchable by email (non-verified keys are
   only searchable by their full fingerprint).

#. **Check your email inbox**: you should have received an automated email from
   keyserver@keys.openpgp.org with **a link to confirm your ownership of the
   key**:

   .. image:: ../img/keyserver_key_upload_3.*

#. **Click on the provided link** - your key is now verified.

   .. image:: ../img/keyserver_key_upload_4.*

#. To make sure that your key was verified properly, you can go to the
   `keyserver home page <KEYSERVER_URL_>`_ and search for the email
   associated with your key. You should get a message saying that a key was
   found for your email address, as shown in the example below:

   .. image:: ../img/keyserver_key_upload_5.*

#. You can now delete the file containing the exported public key on your
   local machine (e.g. ``public_key.pgp``).


.. _section-register-key:

Register your public PGP key in the BioMedIT portal
---------------------------------------------------

.. admonition:: BioMedIT

    **Registering your public PGP key in the** `BioMedIT portal <PORTAL_URL_>`_
    **is mandatory** to be able to use it within the BioMedIT project
    (e.g. create data transfer requests, encrypt data, etc.).

If you are *not* a BioMedIT user, this section is not relevant for you and can
be skipped.

PGP key status
^^^^^^^^^^^^^^

PGP keys used to encrypt, sign and decrypt data transiting via the BioMedIT
network require the approval of the **BioMedIT key validation authority**. The
information of whether a key is trusted or not is stored as **key status** in
the `BioMedIT portal <PORTAL_URL_>`_. This is the reason why all PGP keys used
within BioMedIT must be registered with the BioMedIT portal.

When a PGP key is first registered in the BioMedIT portal, its status is
initially set to ``PENDING`` (i.e. it is awaiting approval). A keys must be
in ``APPROVED`` status before it can be used to encrypt or sign data packages
transiting via the BioMedIT network.

The list of key statuses is as follows:

* ``PENDING``: a key approval request was submitted, but the key has not been
  approved yet. This is a manual process and can take from a few hours or up
  couple of days.
* ``APPROVED``: key is approved for usage within the BioMedIT network. Only
  approved keys can be used to encrypt, sign and decrypt data packages that
  are transiting via the BioMedIT network.
* ``APPROVAL-REVOKED``: approval of the key has been revoked by the BioMedIT
  key validation authority.
* ``KEY-REVOKED``: key has been revoked by its owner.
* ``REJECTED``: key is not trusted by the BioMedIT key validation authority.
* ``DELETED``: key has been removed from the keyserver by its owner.
* ``UNKNOWN KEY``: key has not been registered on the BioMedIT portal. If it
  is your own key, please register it. If it is the key of someone else, please
  ask them to register their key.

To verify that a key is trusted, *sett* connects to the BioMedIT portal and
retrieves the status of the key. For this reason, it is important that BioMedIT
users **register their PGP key with the BioMedIT portal**.

In cases where *sett* is used outside of the BioMedIT project, or the portal
is not reachable, *sett* can still be used to encrypt, decrypt, and transfer
data. In this case, you need to uncheck ``[ ] Verify DTR`` and
``[ ] Verify key approval`` in the **Settings** tab.

The status of a public key in the BioMedIT portal can be easily checked in the
``sett-gui`` application by going to the **Keys** tab, and selecting the key in
the **Public key** list.

Register your public PGP key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. important::

    Each BioMedIT user can only have **1 active PGP key registered** in
    the `BioMedIT portal <PORTAL_URL_>`_ at any time.

    If you wish to replace your currently active key with another one, please
    connect to the `BioMedIT portal <PORTAL_URL_>`_, go to the **"My Keys"**
    tab and click on the **"Retire Key"** icon (Actions column) associated
    with your key.

.. important::

    The **email** associated with your PGP key **must be the same as the email
    that you are using with the BioMedIT portal**.

    If the email associated to your PGP key is different (e.g. a shared
    service email is used for your PGP key), please contact the BioMedIT
    support.

To register a new PGP key with the BioMedIT portal, proceed as follows:

#. **Make sure you have successfully uploaded your key to the keyserver** and
   that you have **completed the email verification** procedure with the
   keyserver (i.e. your key must be *verified* with the keyserver).

#. To make sure that your key is present on the keyserver and is verified, go
   to the `keyserver home page <KEYSERVER_URL_>`_ in your browser and
   **search for the email associated with your key**. You should get a message
   saying that a key was found for your email address, as shown in the example
   below.

   Your key fingerprint will also be shown - see the right-most part of the
   link in this screenshot:

   .. image:: ../img/keyserver_key_upload_5.*

#. **Copy the fingerprint** (40-character string) of your key.

#. **Log in** to the `BioMedIT portal <PORTAL_URL_>`_.

#. Go to the **My Keys** tab.

#. Click on the **"+ KEY"** button, a dialog box will open.

   *Note*: if the button is missing, it is probably because you already have
   an active key in the portal. Each user can only have 1 active key at a
   time - see the information box above.

#. Enter your full key fingerprint (must be exactly 40 characters long) in the
   dialog box, then press the green search icon to the right.

   This will retrieve the **user ID** and **email address** associated with the
   fingerprint from the keyserver and display them in the dialog box.

#. **Verify the user ID and mail address**. If they are correct for your PGP
   key, then click on **Confirm**.

#. A request to approve your key has now been sent to the BioMedIT key
   validation authority. Generally requests are processed quickly (in a matter
   of hours), but occasionally it might take slightly longer as this is a
   manual process.

   Please contact the BioMedIT support if your key has not been approved after
   a couple of days.


Download public PGP keys from the default keyserver
---------------------------------------------------

In order to encrypt data for a specific recipient (who will be able to decrypt
it), you will need to have the public PGP key of that recipient(s) available
in your local keyring. *sett* allows user to easily search and retrieve public
PGP keys from the keyserver specified in the
:ref:`configuration file<Configuration options>`.

.. admonition:: BioMedIT

  By default, *sett* is configured to search, upload and download keys from
  the public keyserver at `<https://keys.openpgp.org>`_. If your configuration
  still points to the former keyserver (keyserver.dcc.sib.swiss), please change
  it to ``https://keys.openpgp.org``.


Download PGP keys with sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To download a public PGP key from the keyserver:

#. In the **Keys** tab, click on **Download keys**. A dialog box will open,
   allowing to search for public keys stored on the keyserver.

#. In the search field, enter either the user email address or fingerprint of
   the public key you are looking for and hit **Search**: the matching keys
   found on the keyserver will be displayed.

   .. image:: ../img/sett_download_key_01.*

   For instance, if searching for the key "Bob Testkey <bob@example.com>" with
   fingerprint "AEED7A96F339881F6FE8291464A1E0150613807D", one can search for
   either "bob@example.com" or "AEED7A96F339881F6FE8291464A1E0150613807D".

#. When you are confident that the key is genuine, select it and click on
   **Download**. You should now see your recipient's key listed in the
   **Public keys** box.

#. Select the newly downloaded key in the **Public keys** list and verify that
   it is marked as **"This key is approved"**, printed in green.

   **Important:** if the message is red ("This key is not approved"), the
   **status of the key** (indicated at the end of the line) will give more
   details. Please see the :ref:`PGP key status <PGP key status>` section
   for details.


Download PGP keys in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following commands show how to download a public PGP key from the OpenGPG
keyserver in command line. To download keys from a different keyserver, simply
replace the URL of the keyserver.

.. code-block:: shell

    # Search and download key from OpenPGP keyserver.
    # The <search term> can be either email or fingerprint.
    gpg --keyserver https://keys.openpgp.org --search-keys <search term>

    # Example: download the public key of the SPHN Data Coordination Centre.
    gpg --keyserver https://keys.openpgp.org --search-keys B37CE2A101EBFA70941DF885881685B5EE0FCBD3

Alternatively, if the fingerprint of the key to retrieve is known the
``--recv-key`` option can be used to directly download the key without first
searching for it:

.. code-block:: shell

    # Example: download the public key of the SPHN Data Coordination Centre.
    gpg --keyserver https://keys.openpgp.org --recv-key B37CE2A101EBFA70941DF885881685B5EE0FCBD3

.. note::

    If you are behind a proxy, you will need to extend the ``gpg`` command with
    the following *http-proxy* option:
    ``--keyserver-options http-proxy=http://proxy:port``


Remove your public PGP keys from the keys.openpgp.org keyserver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

While it is not possible to remove an actual key from the keyserver, it is
possible to remove all personal identification from it (user ID and email).
Such keys are called **unverified**.

To remove personal information associated with a key, go to the keyserver's
`manage key page <https://keys.openpgp.org/manage>`_, enter the email
associated to the key and click on **Send link**.

.. image:: ../img/keyserver_key_upload_6.*

You will receive an email with further instructions on how to proceed to
remove your key's user ID and email from the keyserver.

.. warning::

    Removing a key's user ID and email from the keyserver makes it no longer
    searchable by email. It remains searchable by its full fingerprint.

    **Important: keys without user ID and email cannot be used within the
    BioMedIT network. Only remove your personal identifying information from
    a key if you are no longer using it.**


Delete PGP keys from your local machine
---------------------------------------

Deleting public and/or secret PGP keys form your local certificate store
**is not possible with _sett_**. This is intentional, and users are encouraged
to **revoke their key** instead.

Should you nevertheless wish to delete a key from your local certificate store
(e.g. because you created a test key that is no longer needed), it is possible
to do so by manually deleting the files containing the certificates using your
file explorer or a shell command.

In the local *sett* certificate store, certificates are stored as files named
after the fingerprint of the certificate's primary key. For instance, a
certificate with fingerprint ``3b17f529665fe012ef54f4a1714fdf98b6e828df`` would
be stored under:

* Public certificate: ``<certstore-path>/pgp.cert.d/3b/17f529665fe012ef54f4a1714fdf98b6e828df``
* Secret certificate: ``<certstore-path>/pgp.cert.d.sec/3b/17f529665fe012ef54f4a1714fdf98b6e828df``

The location of the certificate store (``<certstore-path>`` above) is
operating-system dependent:

* **Linux:** ``~/.local/share/pgp.cert.d`` for public certificates, and
  ``~/.local/share/pgp.cert.d.sec`` for secret certificates.
* **Windows:** ``%UserProfile%\AppData\Roaming\pgp.cert.d`` for public
  certificates, and ``%UserProfile%\AppData\Roaming\pgp.cert.d.sec`` for
  secret certificates.
* **MacOS:** ``~/Library/Application Support/pgp.cert.d`` for public
  certificates, and ``~/Library/Application Support/pgp.cert.d.sec`` for
  secret certificates.


Revoke your PGP key
-------------------

If a private PGP key has been compromised, is no longer usable (e.g. password
is lost), or should no longer be used for any other reason, it must be revoked.

A prerequisite for revoking a PGP key is to have generated a
:ref:`revocation certificate<Revocation certificates>` for it. If the PGP key
to revoke was generated with *sett*, you should in principle already have a
revocation certificate ready to use. If you do not have a revocation
certificate yet, please generate one by referring to the
:ref:`Generate a new key pair in command line<Generate a new key pair in command line>`
section.

.. warning::

    **A revoked key cannot be "un-revoked"**. Only revoke a key if you are
    certain you want to do so. Revoked keys can no longer be used to
    encrypt/decrypt data with *sett*.

Revoke your PGP key with sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To revoke a public key in **sett-gui**:

#. In the **Keys** tab, click on **Import key**.
#. Select the *revocation certificate* for the public key you want to revoke.
   This will import the revocation certificate and immediately revoke the key -
   there is no confirmation requested.

   **Warning:** proceed with caution, **a revoked key cannot be "un-revoked"**.

#. When selecting the revoked key from the **Public keys** list, you should now
   see a red "**key has been revoked**" warning listed under the key's
   signatures. From this point on, the key can no longer be used with *sett*.

   .. image:: ../img/key_management_revoked_key.*

#. If you have previously shared your key via a keyserver (e.g.
   `keys.openpgp.org <KEYSERVER_URL_>`_), you must also re-upload your revoked
   key to that keyserver.

   This will allow other users to update their local copy of your public key,
   informing them that it is no longer valid. To upload your revoked public
   key, select it from the **Public keys** list, then click on **Upload keys**
   to upload it to the keyserver.

   If your key was never present on any keyserver, this step should be skipped.


Revoke your PGP key in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Key revocation is not supported in the *sett* command line and must be done
directly with ``gpg``, as shown in the following example:

.. code-block:: shell

    # Example:
    gpg --import revocation_certificate.asc

After a key has been revoked, it must be uploaded again to any keyserver(s)
where it is present, so that the revocation can be shared with others. This
can be done with *sett* as illustrated in the example below:

.. code-block:: shell

    # Example: a key with fingerprint AD1008B5F0F876630B9AF245AA2BF155EF144550
    # was revoked and must now be uploaded to the keyserver.
    sett verify-keylengths-and-upload-keys AD1008B5F0F876630B9AF245AA2BF155EF144550


.. _section-migrate-keys:

Migrating keys from the GnuPG keyring to the sett certificate store
-------------------------------------------------------------------

Starting from version 4.4.0, *sett* no longer uses `GnuPG <GNUPG_URL_>`_ as
default encryption backend. Instead it uses an embedded library -
`Sequoia <SEQUOIA_URL_>`_ - and its own keystore (reverting to using GnuPG is
however possible by enabling the ``legacy_mode`` option in the in the
:ref:`configuration file<section-sett-config-file>` or
"Use GnuPG legacy mode" in the settings tab of *sett-gui*).

As a result, Open PGP keys located in your GnuPG keyring are no longer
automatically detected by *sett*, and they must be migrated to the new *sett*
certificate store.

If you are a user with PGP keys in your GnuPG keyring that you are using with
*sett*, then these keys must be migrated to the new *sett* certificate store.
Using ``sett-gui`` (the GUI version of sett), this procedure is
straightforward:

#. In sett GUI, go to the **Keys tab**.
#. Click on the **Migrate secret key from GnuPG** icon (pictogram of a
   square duplicating itself).
#. A pop-up should open, allowing you to **select the secret PGP key** to
   migrate.
#. **Enter the password** associated with your secret PGP key, then click on
   **Migrate key**.
#. Done. Your secret PGP key has now been imported into *sett*'s certificate
   store and you should see it listed under in the **Private keys** and
   **Public keys** boxes.
#. To migrate public keys (i.e. keys from other people), please simply
   re-download them from the keyserver by clicking on
   **Download keys from keyserver** (pictogram of a could with arrow pointing
   down).

Note: migrating keys from the GnuPG keyring to the *sett* certificate store is
currently not possible in command line mode (sett CLI).

The location of the *sett* certificate store is the following:

* **Linux:** ``~/.local/share/pgp.cert.d`` for public certificates, and
  ``~/.local/share/pgp.cert.d.sec`` for secret certificates.
* **Windows:** ``%UserProfile%\AppData\Roaming\pgp.cert.d`` for public
  certificates, and ``%UserProfile%\AppData\Roaming\pgp.cert.d.sec`` for
  secret certificates.
* **MacOS:** ``~/Library/Application Support/pgp.cert.d`` for public
  certificates, and ``~/Library/Application Support/pgp.cert.d.sec`` for
  secret certificates.

.. _intro_to_pgp:

Introduction to public-key cryptography, PGP and GnuPG
------------------------------------------------------

Public-key cryptography is a method for secure communication between two or
more users. In this system, each user has a pair of unique keys consisting of
a **private key** and a **public key**.
Public and private keys are linked in the sense that, data encrypted with a
given public key can only be decrypted with the matching private key, and data
signed with a given private key will only be recognized by the matching public
key.

Because these keys are based on the `OpenPGP <OPENPGP_URL_>`_ protocol, they
will here be referred to as **PGP keys**.

Public and private PGP keys:

* **Public keys** are used to encrypt data, as well as for verifying signatures
  made on files or emails. By design, public keys are intended to be shared
  with other people and therefore no particular effort is required to keep them
  secret.
  In fact, public keys are often uploaded to public servers, known as
  **keyservers**, where they are accessible to anyone. No password is required
  to use a public key.

  Typically, public keys are used by data senders to encrypt data for one or
  more recipient(s), and by data recipients to verify signatures of files or
  emails (to ensure the sender is genuine).

* **Private keys**, sometimes also referred to as a **secret keys**, are used
  to decrypt data, sign files and sign other people's public keys. To increase
  security, private keys should always be password protected.

  .. important::

      **Private keys and their password must be kept secret at all times.
      Never share your private key or password with anyone.**

      Private keys should be stored in a directory only accessible by the
      key owner, and their password should be stored in a password manager.

  Typically, private keys are used by data recipients to decrypt data, and by
  data senders to sign the files they encrypt.

  .. important::

      If anyone else than the legitimate owner has access to a private key
      and/or its password, **the key pair is considered compromised**.
      It :ref:`must be revoked<Revocation certificates>`, and never used again.

  .. warning:: **GPG security warning**

     The GnuPG software keeps key passwords entered by the users temporarily
     stored for the duration of a user's session - i.e. until the user logs
     out of a session (sometimes the duration of the temporary storage is also
     limited in time).
     This is done for convenience reasons, so that users do not always have
     to re-enter their password.

     A security implication is that if someone has physical access to your
     computer, that person might now be able to use your private key without
     having to know its password.

*sett* uses the open source implementation of public-key cryptography provided
by the GNU Privacy Guard software (**GPG** or **GnuPG**, in short). GnuPG is a
command line tool developed by the Free Software Foundation that uses the
**PGP** `OpenPGP <OPENPGP_URL_>`_ implementation to encrypt and decrypt files.

A detailed documentation of PGP and GnuPG can be found in this online
`manual <https://www.gnupg.org/gph/en/manual.html>`_.

It is also possible - and often desirable - to both encrypt and sign a file.
This ensures that the data can only be read by the intended recipient, and
that the recipient can be confident the sender is legitimate.
This is precisely what *sett* does:

* **Encrypting files**, so that only the intended recipient(s) can read them.
* **Signing files**, so that the recipient(s) can trust the sender is genuine.


Key fingerprints
^^^^^^^^^^^^^^^^

Each pair of public/private PGP keys is identified by a unique **fingerprint**.
Fingerprints are 40 characters long hexadecimal strings (digits and upper case
A-F letters) that look like this::

    238565936FCFF3F200219990941A3EC20555F781

Since nothing is preventing two PGP keys to have the same user name and email
address, it is critical that users **always verify the genuineness of new
keys** before (or just after) importing them into their local keyring (i.e.
their local PGP key database).

Ensuring a key is genuine can be done in two different ways:

* Ask the key owner to **provide their key's fingerprint via a trusted
  communication channel** (e.g. over the phone), and then verify that the
  fingerprint of the newly imported key does indeed match the fingerprint
  communicated to you by its owner.

* Using *sett-gui*, verify that the :ref:`key status<PGP key status>` of the
  key is **APPROVED** (can only be checked after you imported the key).

File encryption
^^^^^^^^^^^^^^^

In public key cryptography, the sender **encrypts** a file using one or more
recipient(s) public key(s).
Once a file is encrypted, no one can read the file without having access to a
private key that matches the public key(s) used for encryption. This ensures
that only the intended recipient(s) can decrypt the file, because they are the
only one to have access to the matching private key.

File signing
^^^^^^^^^^^^

The objective of **file signing** is to guarantee to the recipient of a file
(or email) that the sender is genuine, and not someone else trying to
impersonate the sender.

To achieve this, the sender signs the file with their private key (password
required), and shares their public key with the recipient (typically via a
keyserver).
The recipient can then validate the authenticity of the signature using the
public key of the sender. Since public keys are non-sensitive, they can be
distributed publicly. In fact they are intended for this purpose, hence their
name.

Revocation certificates
^^^^^^^^^^^^^^^^^^^^^^^

In the unfortunate event that a user either i) forgets their private key's
password or ii) have their private key and password stolen/compromised, they
will need a way to let other people know that their public key should no longer
be trusted and used.

This is because:

* If the password was forgotten: the key owner won’t be able to decrypt data
  anymore.
* If the private key was compromised: someone else might be able to decrypt
  data encrypted with the public key, and to illegitimately sign files!

This situation is what **revocation certificates** are for: by applying a
revocation certificate to a public key, and then sharing the revoked key with
others (e.g. via a keyserver), the key owner signals that their key is now
"revoked" and should no longer be trusted nor used. After a key has been
revoked, it can no longer be used to encrypt/decrypt data with *sett*.

Revocation certificates can be generated at anytime from the private key, but
the best practice is to generate them directly after a new key pair is created.
This ensures that the revocation certificate will be available even if the
private key or its password is lost.

.. note::

    *sett-gui* will automatically generate a revocation certificate each time
    a new key is created.

Since anyone with access to a revocation certificate will be able to revoke the
associated key, revocation certificates must be stored securely - e.g. in a
password manager - and should never be shared with anyone.

.. important::

    * Revocation certificates **must be stored in a safe location** (e.g. a
      password manager) and **never shared with anyone**.
    * It is best to generate a revocation certificate **immediately after**
      a new public/private key pair is created.
    * The actual procedure to revoke a key is detailed in the
      :ref:`Revoke your PGP key<Revoke your PGP key>` section.

Exchanging public keys via a keyserver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Encrypting files for a specific recipient requires to have the recipient's
public key in one's local **keyring** (a keyring is a local database
containing PGP keys). Similarly, verifying a signature on a file or a public
key requires to have the signee's public key available in one's local keyring.

Public keys are not sensitive data, and therefore can in principle be sent
unencrypted via email. However, when having frequent key exchanges between
multiple actors, sending public PGP keys around by email quickly becomes
cumbersome. A solution to this problem is using a so called **keyserver** to
share public keys. Keyservers are public or private servers whose sole purpose
is to store public PGP keys and allow users to search for them. In addition,
public keyservers often form a network and are regularly synchronized among
themselves to ensure redundancy.
Private keyservers have the same role, but do not share/synchronize the stored
public keys with any other server, and possibly have a restricted access policy
so that only authorized people can search for keys.

.. admonition:: BioMedIT

    Within the BioMedIT project, **all exchanges of public keys are done via
    the public keyserver at** `https://keys.openpgp.org <KEYSERVER_URL_>`_.
    This keyserver can be accessed directly from the *sett* application
    (recommended), via a web browser, or through command line programs such
    as ``gpg``.

    **New:** they keyserver used to exchange public PGP keys within the
    BioMedIT project has been changed in September 2022. If you have used
    *sett-gui* before, make sure you have set the *Keyserver URL* in the
    *Settings* tab set to ``https://keys.openpgp.org``. Updating to the latest
    version of *sett* is also required and will modify this setting
    automatically for you.


Troubleshoot the "Network is unreachable" and "No keyserver available" error
----------------------------------------------------------------------------

On Mac OS Big Sur and possibly above, the ``gpg`` command to upload or
download keys might end up in a ``Network is unreachable`` error.

To solve this issue, try the following commands:

.. code-block:: shell

    echo "standard-resolver" >>  ~/.gnupg/dirmngr.conf
    gpgconf --reload dirmngr

Since ``gpg`` version 2.1, a daemon called ``dirmngr`` takes care of accessing
the OpenPGP servers.
The configuration above forces ``dirmngr`` to use the system’s standard DNS
resolver code. The second command restarts the ``dirmngr`` daemon, which will
then pick up the new configuration.