DEPRECATED AND UNMAINTAINED VERSION --- sett - secure encryption and transfer tool
==================================================================================

|version| |license| |source| |python|

.. important::

    **DEPRECATION NOTICE --- UNMAINTAINED PROJECT --- DEPRECATION NOTICE**

    This version of **sett** and its associated documentation
    **is not maintained anymore**. Please do not use it anymore and switch to
    the `new version of sett <https://gitlab.com/biomedit/sett-rs>`_ instead.

    The new version of **sett** can be found at
    https://gitlab.com/biomedit/sett-rs, and the associated documentation is
    hosted `here <https://biomedit.gitlab.io/docs/sett/>`_. We strongly
    encourage all users to migrate to the new version for improved performance
    and security.

    This documentation remains available for reference only.

    **DEPRECATION NOTICE --- UNMAINTAINED PROJECT --- DEPRECATION NOTICE**

.. toctree::
  :maxdepth: 3
  :caption: Table of Contents

  overview.rst
  quick_start.rst
  installation.rst
  key_management.rst
  generating_ssh_keys.rst
  usage.rst
  automatization.rst
  config_options.rst
  packaging_specifications.rst
  benchmarks.rst
  faq.rst
  source_location.rst


.. |version| image:: https://img.shields.io/pypi/v/sett.svg?style=flat-square&label=latest%20version
    :target: https://pypi.org/project/sett
    :alt: Latest stable released

.. |license| image:: https://img.shields.io/badge/License-GPLv3-blue.svg?style=flat-square&color=green&label=license
    :target: https://www.gnu.org/licenses/gpl-3.0
    :alt: License

.. |source| image:: https://img.shields.io/badge/source_code-GitLab-orange?style=flat-square&logo=Git&logoColor=white
    :target: https://gitlab.com/biomedit/sett
    :alt: Source code on GitLab

.. |python| image:: https://img.shields.io/pypi/pyversions/sett.svg?style=flat-square&logo=python&logoColor=white&label=supported%20python%20versions&color=purple
    :target: https://pypi.org/project/sett
    :alt: Supported python versions
