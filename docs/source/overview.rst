What is sett?
=============

**sett** stands for "Secure Encryption and Transfer Tool" and is a python-based
wrapper around ``GnuPG`` and ``SFTP`` that automates the packaging, encryption,
and transfer of data.

**sett** is available both as a GUI program ``sett-gui``, and in command line
``sett``. Most functionality of *sett* is available in both the GUI and the
command line.

**sett** is developed as part of the `BioMedIT <https://sphn.ch/network/projects/biomedit>`_
project. It is licensed under the GPLv3 (GNU General Public License) and the
source code is available from its public `GitLab repo <https://gitlab.com/biomedit/sett>`_

.. admonition:: BioMedIT

    When using **sett** to transfer data into the **SPHN BioMedIT network**, a
    number of additional constraints apply. These specific instructions are
    highlighted throughout this documentation in **BioMedIT labeled boxes**,
    just **like this one**.

.. important::

    **IMPORTANT NOTICE - PLEASE READ CAREFULLY**

    Starting with **version 4.4.0**, *sett* has been updated to no longer
    use GnuPG as encryption backend by default.

    **WHAT YOU NEED TO DO:**

    * **If you are a new sett user** with no PGP key (the cryptographic
      key used for data encryption, decryption and signing), you can ignore
      this message.
    * **If you were already using earlier sett versions**, you will need to
      migrate the PGP key(s) you are using with `sett` to the new certificate
      store. Please refer to the instructions given in the section
      :ref:`Migrating keys from the GnuPG keyring to the sett certificate store<section-migrate-keys>`.

    **WHAT ELSE CHANGES FOR YOU AS A USER:**

    * After you migrated your key(s) to the new *sett* certificate
      store, nothing else changes - simply keep using *sett* as you
      did before.

    **MORE DETAILS ABOUT THIS CHANGE**

    * Starting with version 4.4.0, *sett* no longer uses GnuPG as its default
      cryptographic backend. Instead, it now uses an embedded cryptographic
      library named `sequoia<https://sequoia-pgp.org>` and its own keystore.
    * As a result, the Open PGP keys located in your GnuPG keyring are no
      longer automatically detected by *sett*, and they must therefore be
      migrated to the new *sett* certificate store.
    * It remains possible to switch back to using GnuPG as cryptographic
      backend by enabling the ``legacy_mode`` option in the in the
      :ref:`configuration file<section-sett-config-file>` or
      "Use GnuPG legacy mode" in the settings tab of *sett-gui*.
