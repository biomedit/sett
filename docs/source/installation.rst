.. _installing-sett-page:

Installing sett on your local computer
======================================

The Secure Encryption and Transfer Tool - or *sett* runs on all major operating
systems. Please refer to the relevant section below depending on your operating
system.

*sett* requires the following dependencies to be installed on your system:

* `python <https://www.python.org>`_ >= 3.8

Additionally, *sett* also requires users to have:

* A public/private `PGP key pair <https://www.openpgp.org/about>`_. See
  :ref:`PGP key management with sett<PGP key management with sett>` for
  detailed instructions.
* The public PGP key of users for whom they want to encrypt data (i.e. users
  to whom they want to send data).
* For users who intend to transfer data via SFTP, a
  `SSH key <https://www.openssh.com>`_. See
  :ref:`Generating SSH keys<Generating SSH keys>` for detailed instructions.

*Note:* running *sett* in **legacy mode** additionally requires the user to
have `GPG (Gnu Privacy Guard) <https://gnupg.org>`_ >= 2.2.8 installed on their
system.

Installing dependencies
-----------------------

Installing dependencies on Windows
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Download the installer for the latest python 3 version from
   https://www.python.org.

#. Run the downloaded installer file. In the installation wizard, make sure to
   select the **Add Python to PATH** option, as shown in the figure below.

   .. image:: ../img/sett_install_python_win_01.*

#. Reboot your machine.

**Installing optional dependency GnuPG:** this is only required when running
*sett* in **legacy mode**:

#. Download the latest version of GPG for windows (gpg4win) from
   https://www.gpg4win.org.

#. Run the installer and accept all default settings (keep clicking "next").
   At the end of the installation, a prompt might ask to to reboot the
   operating system to complete the install.

   .. image:: ../img/sett_install_gpg4win_01.*

#. Reboot your machine.

Installing dependencies on Linux
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Python 3 can be installed from most Linux distributions' software repositories.
Here are commands for some of the most popular distributions:

* Ubuntu, Debian: ``sudo apt-get install python3 python3-pip``
* Fedora: ``sudo dnf install python3 python3-pip``
* CentOS 7: ``sudo yum install python36 python36-pip``

**Installing optional dependency GnuPG:** this is only required when running
*sett* in **legacy mode**:

* Ubuntu, Debian: ``sudo apt-get install gnupg2``
* Fedora: ``sudo dnf install gnupg2``
* CentOS 7: ``sudo yum install gnupg2``

Installing dependencies on Mac OS
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In most cases, Python will be pre-installed on Mac OS systems. To check whether
it's installed or not, go to **Applications > Utilities** and click on
**Terminal**. Alternatively, click **command + spacebar** on the keyboard,
type ``terminal``, and then click **Enter**.

A terminal window should now have opened. To find out which version of Python
is installed, use the following commands:

.. code-block:: shell

    # Check version of default python:
    $ python --version
    Python 2.7.16

    # Check whether python3 is installed:
    $ python3 --version
    Python 3.X.X


**Installing Python 3 and GnuPG:** the easiest way to install python 3 (if
needed) is using **Homebrew** - https://brew.sh. Instructions to install
Homebrew can be found on its website.
Note that a prerequisite for Homebrew is Apple’s xcode tools
(``xcode-select --install``). With Homebrew available on your system,
installing Python 3 is now a one-liner::

    $ brew install python3

**Installing optional dependency GnuPG:** this is only required when running
*sett* in **legacy mode**::

    $ brew install gpg

Installing sett
---------------

.. note::

    On some older operating systems (e.g. CentOS 7) *sett-gui* might not run
    due to the outdated system libraries.
    In such cases it is necessary to run ``pip install sett[legacy]`` instead
    of ``pip install sett`` (see the instructions below).

.. note::

    If you encounter any error during the installation or when trying to start
    *sett*, please have a look at :ref:`the FAQ section<faq-page>` to see
    whether your problem does already have an answer.

Installing sett on Windows
^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Start a Windows shell by pressing the ``Windows key``, then type ``cmd``
   on your keyboard and press ``enter``.

#. In the Windows shell, run the following command:
   ``pip install sett``.
   If you are accessing the internet via a proxy, please refer to
   :ref:`this section<sett_behind_proxy-section>` for further instructions.

#. To check that *sett* has been correctly installed, try to type:
   ``sett --help`` in the terminal. The *sett* help should be displayed.

#. To start the *sett* application in graphical mode, type ``sett-gui`` in the
   Windows terminal.

   Alternatively, depending on the Python version installed, 
   an executable can be found under
   ``C:\Users\%username%\AppData\Local\Programs\Python\Python38\Scripts\sett-gui.exe``
   or 
   ``C:\Users\%username%\AppData\Roaming\Python\Python310\Scripts\sett-gui.exe``
   or a similar location.

   Note that to navigate to this location using the Windows explorer, you might
   have to enable **show hidden files** in the Windows explorer menu.
   Alternatively you can copy/paste
   ``C:\Users\%username%\AppData\Local\Programs\Python\Python38\Scripts`` or 
   ``C:\Users\%username%\AppData\Roaming\Python\Python310\Scripts`` in
   the Windows explorer bar.

   For convenience, a shortcut to *sett* can be added to the Windows start
   menu: right-click on the ``sett-gui.exe`` file, then select **Pin to
   Start**. *sett* can now be run directly from the Windows start menu.

   .. image:: ../img/sett_install_windows_pin_to_start.*

Installing sett on Linux
^^^^^^^^^^^^^^^^^^^^^^^^

With python 3 installed on your machine, the *sett* application can be
installed by typing the following command in the terminal::

    pip install --user sett

If you are accessing the internet via a proxy, please refer to
:ref:`this section<sett_behind_proxy-section>` for further instructions.

.. note::

    If you have both python 2 and python 3 installed on your machine, you may
    have to use ``pip3`` instead of ``pip`` in the commands above.

.. note::
    Without the ``--user`` option, the above instruction will install the
    python scripts into system space, which will require administrator
    privileges. Adding ``--user``, installs the module into a platform
    dependent user directory (e.g. ``~/.local/bin/`` in the case of Linux).

To check that *sett* has been correctly installed, type ``sett --help`` in the
terminal. The *sett* help should be displayed. To start the app in graphical
mode, type ``sett-gui`` in the terminal.

Installing sett on Mac OS
^^^^^^^^^^^^^^^^^^^^^^^^^

Follow the same installation instructions as found in the Linux section above.

If you experience SSL certification error messages when running *sett*, such
as::

    certificate verify failed: unable to get local issuer certificate.

or::

    Could not connect to pypi repository to query the latest version.

you will need to execute the file ``Install Certificates.command``,
found in your ``Python 3.X`` application directory. Typically the file is
located in ``~/Applications/Python 3.X/Install Certificates.command``.

.. note::

    In case of a system wide python installation, the
    ``Install Certificates.command`` file will be located in
    ``/Applications/Python 3.X/Install Certificates.command``.

    In this scenario, running the file will also require root privileges.


Installing sett in a Conda virtual environment [cross-platform]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Conda package manager can be used to create a virtual, sandboxed,
environment in which *sett* can then be installed:

.. code-block:: shell

    conda create -y --name sett_venv --no-default-packages python=3.8
    conda activate sett_venv
    pip install sett

    sett -h              # Display sett help to check it was properly installed.
    sett-gui             # Start sett in graphical mode.
    conda deactivate     # When you are done using sett, close the conda environment.

**Removing the conda environment:** to delete the Conda environment where
*sett* is installed, the following command can be used:

.. code-block:: shell

    conda env remove --name sett_venv

**Change install location:** by default, Conda installs the ``sett_venv``
environment into the user's home directory. To specify an alternative
location, replace ``--name sett_venv`` with
``--prefix /alternative/path/sett_venv``, as shown in this example:

.. code-block:: shell

    conda create -y --prefix /alternative/path/sett_venv --no-default-packages python=3.8
    conda activate /alternative/path/sett_venv
    conda env remove --prefix /alternative/path/sett_venv


Installing a specific version of sett
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Specific versions of *sett* can be download from
https://gitlab.com/biomedit/sett/-/releases. After having unpacked the
downloaded file and changed the working directory into the root of the
unpacked files, the installation can be done with the command:

.. code-block:: shell

    pip install --user .

For proxy settings please refer to :ref:`this section<sett_behind_proxy-section>`.


Updating sett
-------------

As only the latest version of *sett* is officially supported and guaranteed
to work, it is strongly recommended to always keep your local installation of
*sett* up-to-date.

In most cases, the following command can be used to upgrade *sett*::

    pip install --upgrade --user sett

For proxy settings please refer to :ref:`this section<sett_behind_proxy-section>`.

.. note::

    If you have both python 2 and python 3 installed on your machine, you may
    have to use ``pip3`` instead of ``pip`` in the command above.

    If you have installed *sett* in a Conda virtual environment, make sure the
    environment is activated before running the upgrade command.

If you have not installed *sett* yourself, please kindly ask your administrator
to perform the upgrade for you.
